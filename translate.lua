local ngettext


if core.get_modpath("intllib") then
	if intllib.make_gettext_pair then
		-- New method using gettext.
		modAirBoat.translate, ngettext = intllib.make_gettext_pair()
	else
		-- Old method using text files.
		modAirBoat.translate = intllib.Getter()
	end
elseif core.get_translator ~= nil and core.get_current_modname ~= nil and core.get_modpath(core.get_current_modname()) then
	modAirBoat.translate = core.get_translator(core.get_current_modname())
else
	modAirBoat.translate = function(s) return s end
end
