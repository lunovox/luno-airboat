

modAirBoat.getFormSpecs = {
	main_panel = function(self)
		
		local btnTongleDoor = {
			label = modAirBoat.translate("Open Door to Passengers"),
		}
		if self.passengers.opened_door then
			btnTongleDoor.label = modAirBoat.translate("Close Door to Passengers")
		end
		
		local btnStart = {
			id = "btnEngineStart",
			label = modAirBoat.translate("Start Engine")
			
		}
		if self.started then  -- if true show button off
			btnStart = {
				id = "btnEngineStop",
				label = modAirBoat.translate("Stop Engine")
			}
		end
		btnStart.button_color = "#FFFFFF"
		btnStart.button_type = "button_exit"
		if type(self.fuel)=="number" and self.fuel <= 0 then
			btnStart.button_color = "#FFFF00"
			btnStart.button_type = "button"
		end
		
		local btnSoundHorn = {
			button_color = "#666666",
		}
		if modAirBoat.horn_enabled then
			btnSoundHorn.button_color = "#FFFFFF"
		end
		
		local btnPanelLeave = {
			button_type = "button_exit",
			button_color = "#FFFFFF",
			button_label = modAirBoat.translate("Leave from the Airboat"),
		}
		local floorPos = self.object:get_pos()
		if floorPos and modAirBoat.getFloor(floorPos) == "air" then
			btnPanelLeave.button_type = "button"
			btnPanelLeave.button_color = "#FFFF00"
			btnPanelLeave.button_label = modAirBoat.translate("Unsafe Leave from the Airboat")
		end
		
		
		local btnCruizerMode = {
			button_state = modAirBoat.translate("OFF"),
			button_type = "button_exit"
		}
		if self.cruizer_mode then
			btnCruizerMode.button_state = modAirBoat.translate("ON")
			btnCruizerMode.button_type = "button"
		end
		
		
		
		return ""
		.."formspec_version[6]"
		.."size[9,6.00,true]" --true = tamanho fixo (não esticavel)
		--.."anchor[0.5,0.0]" -- faz o ponto de referência do formulário formulário ficar no centro do topo do formulário.
		.."position[0.55,0.37]" -- faz o ponto de referência do formulário ficar encaixado no topo central da janela.
		--.."padding[0.0,0.0]"
		--.."no_prepend[]"
		.."bgcolor[#08080844;true]" --Padrão: #080808BB
		.."background[-3.50,-1;15,9;hud_panel.png]"
		.."box[0.25,-0.1;10.00,0.6;#00000088]"
		.."label[0.50,0.25;"..
			core.formspec_escape(
				core.get_color_escape_sequence("#FFFFFF")
				..modAirBoat.translate("AIRBOAT CONTROL PANEL [@1]", (self.license_plate or "???"))
			)
		.."]"
		.."button[0.25,0.75;9.00,0.75;btnUserManual;"
			..core.formspec_escape(
				core.get_color_escape_sequence("#FFFFFF")
				..modAirBoat.translate("User Manual")
			)
		.."]"
		.."button[0.25,1.50;9.00,0.75;btnTongleDoor;"
			..core.formspec_escape(
				core.get_color_escape_sequence("#FFFFFF")
				..btnTongleDoor.label
			)
		.."]"
		..btnCruizerMode.button_type.."[0.25,2.25;9.00,0.75;btnTongleCrizerMode;"
			..core.formspec_escape(
				core.get_color_escape_sequence("#FFFFFF")
				..modAirBoat.translate("Cruizer Mode [@1]", btnCruizerMode.button_state)
			)
		.."]"
		.."button[0.25,3.00;4.50,0.75;btnGearboxLevelUp;"
			..core.formspec_escape(
				core.get_color_escape_sequence("#FFFFFF")
				..modAirBoat.translate("Gearbox Level Up")
			)
		.."]"
		.."button[4.75,3.00;4.50,0.75;btnGearboxLevelDown;"
			..core.formspec_escape(
				core.get_color_escape_sequence("#FFFFFF")
				..modAirBoat.translate("Gearbox Level Down")
			)
		.."]"
		..btnStart.button_type.."[0.25,3.75;4.50,0.75;"..btnStart.id..";"
			..core.formspec_escape(
				core.get_color_escape_sequence(btnStart.button_color)
				..btnStart.label
			)
		.."]"
		.."button[4.75,3.75;4.50,0.75;btnSoundHorn;"
			..core.formspec_escape(
				core.get_color_escape_sequence(btnSoundHorn.button_color)
				..modAirBoat.translate("Sound Horn")
			)
		.."]"
		.."button[0.25,4.50;4.50,0.75;btnImplements;"
			..core.formspec_escape(
				core.get_color_escape_sequence("#666666")
				..modAirBoat.translate("Implements")
			)
		.."]"
		.."button[4.75,4.50;4.50,0.75;btnSetting;"
			..core.formspec_escape(
				core.get_color_escape_sequence("#666666")
				..modAirBoat.translate("Settings")
			)
		.."]"
		..btnPanelLeave.button_type.."[0.25,5.25;9.00,0.75;btnCheckLeave;"
			..core.formspec_escape(
				core.get_color_escape_sequence(btnPanelLeave.button_color)
				..btnPanelLeave.button_label
			)
		.."]"
	end,
}

--######################################################################################

modAirBoat.getFormSpecs.user_manual = function(self)
	if type(self.drivername)=="string" and self.drivername ~= "" then
		local body_template = modAirBoat.getFileContent("locale/manuals/user_manual.template.html")
		local userInfo = core.get_player_information(self.drivername)
		--local userLanguage = userInfo.lang_code
		local body = modAirBoat.getFileContent("locale/manuals/user_manual."..userInfo.lang_code..".html")
		if type(body)=="nil" or body == "" then
			body = body_template
		end
		--local body = dump(userInfo)
		return ""
		--.."formspec_version[6]"
		.."size[11,7.5]"
		
		.."box[0.0,-0.1;10.75,0.6;#000088]"
		.."label[0.25,0;"..
			core.formspec_escape(
				core.get_color_escape_sequence("#FFFFFF")
				..modAirBoat.translate("USER MANUAL")
			)
		.."]"
		.."hypertext[0.50,0.75;10.5,7;myPage;"
		--..core.formspec_escape("<global margin=10 valign=0 color=#FF00FF hovercolor=#00FFFF size=12 font=normal halign=center >")
		.."<global valign=middle halign=left margin=15 background=#FFFFFF color=#000000 hovercolor=#0000FF size=12 font=normal>"
		.."<tag name=html background=#FFFFFF color=#000000>"
		.."<tag name=body background=#FFFFFF color=#000000>"
		.."<tag name=action color=#FF0000 hovercolor=#0000FF font=normal size=12>"
		..core.formspec_escape(body:format(self.drivername:upper()))
		.."]" -- Fim de hypertext[]
		--.."container_end[]"
		.."button[4.5,7.0;3.0,1;btnMainPanel;"
			..core.formspec_escape(
				core.get_color_escape_sequence("#FFFFFF")
				..modAirBoat.translate("Back")
			)
		.."]"
	end
end

--######################################################################################

modAirBoat.getFormSpecs.door_sideout = function(self, playername)
	local btnEnterLikeDriver = {
		button_color = "#666666",
		button_label = modAirBoat.translate("Enter as a driver"),
	}
	local btnEnterLikePassenger = {
		button_color = "#666666",
		button_type = "button",
	}
	if not self.seized
		and self.passengers.opened_door 
		and type(self.drivername)=="string"	and self.drivername~="" 
		and core.get_player_by_name(self.drivername) --Check if the mororista is still online.
		and self.passengers.name1~=playername --Check if the player was not already seated.
		and self.passengers.name2~=playername --Check if the player was not already seated.
		and ( --Check if there are empty chairs.
			type(self.passengers.name1)=="nil" or self.passengers.name1==""
			or type(self.passengers.name2)=="nil" or self.passengers.name2==""
		)
	then
		btnEnterLikePassenger.button_color = "#FFFFFF"
		btnEnterLikePassenger.button_type = "button_exit"
	end
	
	if not self.seized then
		if self.owner == playername then
			btnEnterLikeDriver.button_color = "#FFFFFF"
		elseif core.get_player_privs(playername).valet_parking then
			btnEnterLikeDriver.button_color = "#FFFF00"
			btnEnterLikeDriver.button_label = modAirBoat.translate("Enter as a Valet Parking")
		end
	end
	
	local btnSeizure = {
		button_label = modAirBoat.translate("Undo Seizure"),
		button_type = "button",
		button_color = "#FFFF00",
	}
	if not self.seized then
		btnSeizure.button_label = modAirBoat.translate("Do Seizure")
		btnSeizure.button_type = "button_exit"
		btnSeizure.button_color = "#FF0000"
	end
	if not core.get_player_privs(playername).police_officer then
		btnSeizure.button_type = "button"
		btnSeizure.button_color = "#666666"
	end
	
	local btnEncapsulate = {
		button_type = "button",
		button_color = "#666666",
	}
	if not self.seized then
		if 
			(
				modAirBoat.getEncapsulater() == "owner" 
				and (self.owner=="" or self.owner == playername)
			) or (
				type(core.get_player_privs(playername)[modAirBoat.getEncapsulater()])=="boolean" 
				and core.get_player_privs(playername)[modAirBoat.getEncapsulater()]==true
			)
		then
			btnEncapsulate.button_type = "button_exit"
			btnEncapsulate.button_color = "#FFFFFF"
		end
	end
	
	return "size[9,5.50]"
	.."box[-0.0,-0.1;8.75,0.6;#000088]"
	.."label[0.25,0;"..
		core.formspec_escape(
			core.get_color_escape_sequence("#FFFFFF")
			..modAirBoat.translate("AIRBOAT ENTRY DOOR [@1]", (self.license_plate or "???"))
		).."]"
	.."button[0,0.75;8.75,1;btnEnterLikeDriver;"
		..core.formspec_escape(
			core.get_color_escape_sequence(btnEnterLikeDriver.button_color)
			..btnEnterLikeDriver.button_label
		)
	.."]"
	..btnEnterLikePassenger.button_type.."[0,1.50;8.75,1;btnEnterLikePassenger;"
		..core.formspec_escape(
			core.get_color_escape_sequence(btnEnterLikePassenger.button_color)
			..modAirBoat.translate("Enter as a pessenger")
		)
	.."]"
	.."button[0,2.25;8.75,1;btnViewIntegrity;"
		..core.formspec_escape(
			core.get_color_escape_sequence("#666666")
			..modAirBoat.translate("View Vehicle Integrity")
		)
	.."]"
	..btnSeizure.button_type.."[0,3.00;8.75,1;btnSeizure;"
		..core.formspec_escape(
			core.get_color_escape_sequence(btnSeizure.button_color)
			..btnSeizure.button_label
		)
	.."]"
	.."button[0,3.75;8.75,1;btnTransferOwnership;"
		..core.formspec_escape(
			core.get_color_escape_sequence("#666666")
			..modAirBoat.translate("Transfer Ownership")
		)
	.."]"
	..btnEncapsulate.button_type.."[0,4.50;8.75,1;btnEncapsulate;"
		..core.formspec_escape(
			core.get_color_escape_sequence(btnEncapsulate.button_color)
			..modAirBoat.translate("Encapsulate in Inventory")
		)
	.."]"
end

--######################################################################################

modAirBoat.getFormSpecs.confirm_eject = function(self, chairtype)
	local formspec = "size[9,3.25]"
	.."box[-0.0,-0.1;8.75,0.6;#000088]"
	.."label[0.25,0;"..
		core.formspec_escape(
			core.get_color_escape_sequence("#FFFFFF")
			..modAirBoat.translate("AIRBOAT EXIT")
		).."]"
	.."label[0.25,1.0;"..core.formspec_escape(
		modAirBoat.translate(
			"The terrain below the Airboat may not be safe."
		)
	).."]"
	.."label[0.25,1.5;"..core.formspec_escape(
		modAirBoat.translate(
			"Do you really want to leave in this condition?"
		)
	).."]"
	
	local btnEject = {
		button_id = "btnEjectOk",
		buttonexit_type = "button",
		buttonexit_id = "btnMainPanel",
	}
	if type(chairtype)=="string" and (chairtype=="passenger1" or chairtype=="passenger2") then
		if chairtype=="passenger1" then
			btnEject.button_id = "btnEjectP1Ok"
		elseif chairtype=="passenger2" then
			btnEject.button_id = "btnEjectP2Ok"
		end
		btnEject.buttonexit_type = "button_exit"
		btnEject.buttonexit_id = "" --You don't need an ID because it will just close!
	end
	formspec = formspec.."button_exit[2.5,2.5;2,1;"..btnEject.button_id..";"..core.formspec_escape(modAirBoat.translate("YES")).."]"
	formspec = formspec.. btnEject.buttonexit_type.."[4.5,2.5;2,1;"..btnEject.buttonexit_id..";"..core.formspec_escape(modAirBoat.translate("NOT")).."]"	
	return formspec
end


