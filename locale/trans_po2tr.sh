#! /bin/bash

# This program is free software; you can 
# redistribute it and/or modify it under 
# the terms of the [GNU AGPL] as published 
# by the Free Software Foundation; either 
# version 3 of the License, or (at your 
# option) any later version. 
# [GNU AGPL]:https://gitlab.com/lunovox/luno-airboat/-/raw/master/LICENSE

LANGUAGES="ca cs da de dv eo es et fr hu id it ja jbo kn lt ms nb nl pl pt pt_BR ro ru sl sr_Cyrl sv sw tr uk"


function printLogo(){
cat << 'EOF'

 _____                      ____   ___ ____ _____ ____
|_   _| __ __ _ _ __  ___  |  _ \ / _ \___ \_   _|  _ \
  | || '__/ _` | '_ \/ __| | |_) | | | |__) || | | |_) |
  | || | | (_| | | | \__ \ |  __/| |_| / __/ | | |  _ <
  |_||_|  \__,_|_| |_|___/ |_|    \___/_____||_| |_| \_\

  @ license [GNU AGPL]

EOF
}

function init(){
	cd .. # init in dir3ctory 'luno-airbost'.
	localTemplate="./locale/airboat.template.pot"
	sisLang=$(echo $LANG | cut -d "." -f 1)
	clear
	printLogo
	PS3="Confirma que '$sisLang' é o código de seu idioma? >>> "
	select ask1 in SIM NÃO; do
		case $ask1 in
	      "SIM")
				selLang=$sisLang
				localPo="./locale/airboat.$selLang.po"
				clear
				printLogo
				echo -e "\nSelected Language: \e[01;36m$selLang\e[0m\n"
				read -s -n 1 #wait any key.
				doSelTask
				break
				;;
	      "NÃO")
				clear
				printLogo
				doSelLang
				;;
	      *)
				clear
				printLogo
				echo -e "\e[01;31m Opção inválida.\e[0m Tente novamente."
				;;
		esac
	done
}

function doSelLang(){
	clear
	printLogo
	PS3=$'\e[01;33m  Selecte your language: \e[0m'
	select selLang in $LANGUAGES; do
		if [ -n "$selLang" ]; then # -z=vazio -n=cheio.
			localPo="./locale/airboat.$selLang.po"
			clear
			printLogo
			echo -e "\nSelected Language: \e[01;36m$selLang\e[0m\n"
			read -s -n 1 #wait any key.
			doSelTask
		else
			clear #Limpa e pergunta denovo!
			printLogo
		fi
	done
}

function doSelTask(){
	clear
	printLogo
	PS3="Selecte your task: "
	select selTask in \
		"Gerate empty translate." \
		"Update translate." \
		"Nothing! Close all."; 
	do
		case $selTask in
	      "Gerate empty translate.")
				doGeratePO
			;;
	      "Update translate.")
				doUpdatePO
			;;
			"Nothing! Close all.")
				closeProgram
			;;
	      *)
				clear
				printLogo
				echo -e "\n\e[01;31m Opção inválida.\e[0m Tente novamente."
			;;
		esac
	done
}

function doGeratePO(){
	if [ -e "$localPo" ]; then
		clear
		printLogo
		echo -e "\n\e[01;33m[ERRO]\e[0m Impossível criar o '\e[01;36m$localPo\e[0m'."
		echo "Motivo: O arquivo já existe!"
		read -s -n 1 #wait any key.
		doSelTask
	fi
	clear
	printLogo
	# To generate file [template.pot], did use terminal command:
	xgettext -n *.lua -L Lua --force-po \
		--keyword=modAirBoat.translate \
		--from-code=UTF-8 -o "$localTemplate"
	echo -e "Gerate Template in '\e[01;36m$localTemplate\e[0m'!"
	
	# Generate file '.po' per [template.pot] file:
	msginit --no-translator --no-wrap --locale="$selLang" \
		--input="$localTemplate" \
		--output-file="$localPo"
	read -s -n 1 #wait any key.
	howEditPO
}

function doUpdatePO(){
	if [ -z "$localPo" ]; then # -z file not exist
		clear
		printLogo
		echo -e "\n\e[01;33m[ERRO]\e[0m Impossível atualizar o '\e[01;36m$localPo\e[0m'."
		echo "Motivo: O arquivo ainda não existe!"
		read -s -n 1 #wait any key.
		doSelTask
	fi
	clear
	printLogo
	# To generate file [template.pot], did use terminal command:
	xgettext -n *.lua -L Lua --force-po \
		--keyword=modAirBoat.translate  \
		--from-code=UTF-8 -o "$localTemplate"
	echo -e "Gerate Template in '\e[01;36m$localTemplate\e[0m'!"
	
	# To Update ".po" file from a [template.pot] file.
	msgmerge --sort-output --no-wrap \
		--update --backup=off \
		"$localPo" "$localTemplate"
	read -s -n 1 #wait any key.
	howEditPO
}

function howEditPO(){
	PS3="Digite o que deseja fazer com '$localPo' : "
	clear
	printLogo
	select selEditor in \
		"Editar graficamente com 'poedit'." \
		"Editar por nano no terminal." \
		"Nada! Editarei com outro programa." 
	do
		case $selEditor in
	      "Editar graficamente com 'poedit'.")
				poedit $localPo
				transformToTr
			;;
	      "Editar por nano no terminal.")
				nano -lS -T 3 $localPo
				transformToTr
			;;
	      "Nada! Editarei com outro programa.")
				closeProgram
			;;
	      *)
				clear
				printLogo
				echo -e "\n\e[01;31m Opção inválida.\e[0m Tente novamente.\n\n"
			;;
		esac
	done
}

function transformToTr(){
	#cd ./locale/
	lua5.3 ./locale/po2tr.lua "airboat" "$localPo"
	#mv "$LANG.tr" "airboat.pt_BR.tr"
	closeProgram
}

function closeProgram(){
	clear
	printLogo
	echo -e "\n\nProgram finalized!\n"
	exit 0
}

init # execute the program from init.
