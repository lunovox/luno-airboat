
core.register_privilege(
	"valet_parking",  {
		description=modAirBoat.translate("Control the vehicle of other owner. Ideal privilege for police officers."), 
		give_to_singleplayer=false,
	}
)--core.get_player_privs(playername).valet_parking

core.register_privilege(
	"vehicle_mechanic",  {
		description=modAirBoat.translate("Allows you to modify and repair vehicles."), 
		give_to_singleplayer=false,
	}
)

core.register_privilege(
	"police_officer",  {
		description=modAirBoat.translate("Allows to seize (blocking the control) of vehicles used in a harmful way."), 
		give_to_singleplayer=false,
	}
)

core.register_privilege(
	"public_notary",  {
		description=modAirBoat.translate("Allows to transfer ownership of a vehicle to another owner."), 
		give_to_singleplayer=false,
	}
)

core.register_privilege(
	"encapsulater",  {
		description=modAirBoat.translate("Allows you to transform the vehicle into an item that fits within the player's inventory."), 
		give_to_singleplayer=false,
	}
)

modAirBoat.getEncapsulater = function()
	return core.settings:get("airboat.encapsulater") or "encapsulater" --Default: "encapsulater"
end

modAirBoat.max_flight_height = tonumber(core.settings:get("airboat.max_flight_height") or 1000) --Default: 1000
modAirBoat.horn_enabled = (core.settings:get("airboat.sound_horn_enabled") ~= "false") --Default: true
modAirBoat.horn_interval = tonumber(core.settings:get("airboat.sound_horn_interval") or 5) --Default: 5
modAirBoat.display_debug = core.settings:get_bool("airboat.debug") --Default: false

local px = modAirBoat.px
local airboat_format = { 
	-- Widmin, heimin, lenmin, widmax, heimax, lenmax
	{-px*13, -px*08, -px*24,     px*13,  px*18,  px*24},  -- Envelope
  --{-px*08, -px*24,   -0.25,    px*08, -px*08,  0.25}, -- Gondola
  
	{-px*01,  px*18, -px*24,     px*01,  px*24, -0.25}, -- Top fin
	{-px*01, -px*14, -px*24,     px*01, -px*08, -0.25}, -- Base fin
	{-0.396,  0.083, -px*24,    -px*13,  0.125, -0.25}, -- Left fin
	{ px*13,  0.083, -px*24,     0.396,  0.125, -0.25}, -- Right fin
	
	{-px*08, -px*24, -px*12,    px*08, -px*23,  px*12}, --Gondola-bottom 
	--{-px*08, -px*24, -px*12,   -px*07, -px*08,  px*12}, --Gondola-left 
	--{ px*08, -px*24, -px*12,    px*07, -px*08,  px*12}, --Gondola-right 
	{-px*08, -px*24, -px*04,    px*08, -px*08, -px*12}, --Gondola-back 
	
	
	{-px*08, -px*24, px*12,    px*08, -px*20, px*11}, --Gondola-from-bottom 
	{-px*08, -px*11, px*12,    px*08, -px*08, px*11}, --Gondola-from-top
	{-px*08, -px*24, px*12,   -px*06, -px*08, px*11}, --Gondola-from-left
	{ px*08, -px*24, px*12,    px*06, -px*08, px*11}, --Gondola-from-right
	
 --{-px*08, -px*24,  px*12,   -px*07, -px*08, -px*12}, --Gondola-left
	{-px*08, -px*11,  px*12,   -px*07, -px*08, -px*12}, --Gondola-left-top
	{-px*08, -px*24,  px*12,   -px*07, -px*08,  px*10}, --Gondola-left-from
	{-px*08, -px*24, -px*02,   -px*07, -px*08, -px*12}, --Gondola-left-back
	{-px*08, -px*24,  px*12,   -px*07, -px*20, -px*12}, --Gondola-left-bottom

 --{px*08, -px*24,  px*12,   px*07, -px*08, -px*12}, --Gondola-right
	{px*08, -px*11,  px*12,   px*07, -px*08, -px*12}, --Gondola-right-top
	{px*08, -px*24,  px*12,   px*07, -px*08,  px*10}, --Gondola-right-from
	{px*08, -px*24, -px*02,   px*07, -px*08, -px*12}, --Gondola-right-back
	{px*08, -px*24,  px*12,   px*07, -px*20, -px*12}, --Gondola-right-bottom
	
	{-px*07, -px*24, px*13,   -px*04, -px*21, px*12}, --light-from-left
	{ px*07, -px*24, px*13,    px*04, -px*21, px*12}, --light-from-right
	

}

modAirBoat.def = {
	-- Custom fields
	license_plate = nil,
	drivername = nil,
	removed = false,
	v = 0,
	vy = 0,
	rot = 0,
	cruizer_mode = false,
	owner="",
	color="white",
	sticker="default",
	odometer = 0, --registers the distances traveled in meters by the airboat throughout its existence.
	fuel = 0, -- =50000ml or 50 coal
	gearshifter = 1,
	started = false,
	seized = false,
	passengers = {
		opened_door = false,
		passengername1 = "",
		passengername2 = "",
	},
	--nametag = "",
	--soundHandles={ },
	config = {
		fuel_max = 50, --default: 50
		fuel_consumption_fly = 0.0001, --default: 0.0001
		fuel_consumption_walk = 0.001, --default: 0.001
		vel_rot = 0.001, --velocity of ratotion. default: 0.001
		vel_walk = 0.2, --velocity of walking from and back. defalt: 0.1
		--vel_max = 15,  --default: 5
		vel_side = 2.0, --velocity of side walkin. default: 0.8
		vel_fly = 0.2, --velocity of fly up/down. default: 0.075
	},
	initial_properties = {
		physical = true,
		collide_with_objects = true, -- Workaround fix for a MT engine bug
		--collisionbox = {-0.85, -1.5, -0.85, 0.85, 1.5, 0.85},
		--collisionbox = {-2.70, -3.85, -2.70,	2.70, 5.62, 2.70},
		--collisionbox = airboat_format[1],
		--[[  ]]
		collisionbox = {
			-modAirBoat.scale*px*20, -modAirBoat.scale*px*36, -modAirBoat.scale*px*20,
			modAirBoat.scale*px*20, modAirBoat.scale*px*27, modAirBoat.scale*px*20 
		}, 
		--]]
		visual = "wielditem",
		visual_size = {x=modAirBoat.scale, y=modAirBoat.scale},
		textures = {"airboat:nodebox_white_default"},
		use_texture_alpha = true, --Entity Value = true | Node Value = "clip"
	}
}

local function get_sign(i)
	if i == 0 then
		return 0
	else
		return i / math.abs(i)
	end
end

local function get_velocity(v, yaw, y)
	local x = -math.sin(yaw) * v
	local z =  math.cos(yaw) * v
	return {x = x, y = y, z = z}
end

local function get_v(v)
	return math.sqrt(v.x ^ 2 + v.z ^ 2)
end

modAirBoat.debug = function(debugtext)
	if modAirBoat.display_debug then
		print("[modAirBoat.debug] "..debugtext)
		core.chat_send_all(debugtext)
	end
end

modAirBoat.getRandomLicensePlate = function(seed)
	local sign = ""
	local chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	math.randomseed(seed)
	for i=1,3 do
		local randNum = math.random(1, #chars)
		local nandChar = chars:sub(randNum, randNum)
		sign = sign..nandChar
	end
	return sign.."."..string.format("%04d",math.random(1, 9999))
end

modAirBoat.doSoundPlay = function(object, soundname, distance, ifHandler, playername, ifLoop)
	if object then
		if soundname and soundname ~= "" then
			if not ifHandler then
				--modAirBoat.debug("object:get_pos() = '"..dump(object:get_pos()).."' | soundname = '"..dump(soundname).."' | playername = '"..dump(playername).."'")
				core.sound_play(
					soundname,
				   {
				   	object = object, 
						gain = 1.0,
						pitch = 1.0,
				   	max_hear_distance = distance or 5, --32 = 2 x chunck_size
				   	loop = false,
				   	
				   },
					true --if ephemeral like https://github.com/minetest/minetest/blob/master/doc/lua_api.md
			   )
		   else
		   	local tmpHandler = core.sound_play(
					{
						name = soundname
					},
				   {
				   	object = object, 
				   	gain = 1.0, --Repeated sounds should be lower than normal.
						pitch = 1.0,
				   	--max_hear_distance = distance or 5, --32 = 2 x chunck_size
				   	max_hear_distance = 0,
				   	loop = (ifLoop and ifLoop == true),
				   }
			   )
		   	if playername and playername ~= "" then
			   	if not modAirBoat.handlerSound[playername] then
			   		modAirBoat.handlerSound[playername] = { }
			   	end
			   	modAirBoat.handlerSound[playername][soundname] = tmpHandler
		   	end
		   end
		end
   end
end

modAirBoat.doSoundStop = function(playername, soundname)
	if type(playername)=="string" and playername~="" then
		if type(soundname)=="string" and soundname~="" then
			if modAirBoat.handlerSound[playername] and modAirBoat.handlerSound[playername][soundname] then
				core.sound_stop(modAirBoat.handlerSound[playername][soundname])
			end
		end
	end
end


modAirBoat.send_message = function(self, playername, message, soundname)
	if type(playername)=="string" and playername~="" then
		message=core.get_background_escape_sequence("#FFFFFF")..
		core.colorize("#00FF00", "["..modAirBoat.translate("AIRBOAT").."]").." "..message
		core.chat_send_player(playername, message)
		
		if self and soundname and type(soundname)=="string" and soundname~="" then
			local player = core.get_player_by_name(playername)
			if player and player:is_player() then
				if type(self.soundHandles) == "nil" then
					self.soundHandles = { }
				end
				if type(self.soundHandles.speaker) ~= "nil" then 
					core.sound_stop(self.soundHandles.speaker)
				end
				self.soundHandles.speaker=core.sound_play(
					soundname, {
						--object = self.object, --toca so em volta.
						--object = player, --Som ruim.
						--to_player = playername, --Toca somente a cabeça de um unico jogador.
						pos = player:get_pos(), --Tocaca cabeca do jogador e em quem estiver proximo 8 blocos (max_hear_distance)
						gain = 1.0, 
						max_hear_distance = 8
					}
				)
			end
		end
	end
end

modAirBoat.getClockFormated = function()
	local circadian = core.get_timeofday()
	local hour = math.floor(circadian * 24)
	local hourpart = (circadian * 24) - hour
	local hourquarter = math.floor((hourpart * 60) / 15)
	local minute = hourquarter * 15
	return string.format("%02d",hour)..":"..string.format("%02d",minute)
end

modAirBoat.getHudText = function(self, player)
	--modAirBoat.getHudText(self, player)
	local lblEnginePower = modAirBoat.translate("OFF")
	if self.started then
		lblEnginePower = modAirBoat.translate("ON")
	end
	local chuncksize = 16
	local pos = self.object:get_pos()
	local x = math.floor(pos.x / chuncksize)
	local dirx = ""
	if x < 0 then
		x = x * -1
		dirx = modAirBoat.translate("W") --OEST/WEST
	else
		dirx = modAirBoat.translate("E") --LESTE/EAST
	end
	
	local z = math.floor(pos.z / chuncksize)
	local dirz = ""
	if z < 0 then
		z = z * -1
		dirz = modAirBoat.translate("S") --SUL/SOUTH
	else
		dirz = modAirBoat.translate("N")	--NORTE/NORTH
	end

	local y = math.floor(pos.y-modAirBoat.scale)
	
	local dir_player = math.floor(360 * player:get_look_horizontal() / (2 * math.pi)) -- Number of turns the airboat takes on the Y axis	
	local turns_airboat = math.floor(self.object:get_yaw()/(2 * math.pi)) -- Number of turns the airboat takes on the Y axis
	local dir_airboat = math.floor((self.object:get_yaw() - (2 * math.pi * turns_airboat)) * 180 / math.pi)
	
	
	--local vel = self.v
	
	local odometer = tonumber(self.odometer)
	--odometer = math.floor(self.odometer/1000)
	odometer = self.odometer/1000
	--modAirBoat.debug("[modAirBoat.getHudText()] self.fuel = "..dump(self.fuel))
	
	return modAirBoat.translate("ENGINE")..(": [%s]"):format(lblEnginePower)
		.."\n"..modAirBoat.translate("GPS")..(": [%02d%s:%02d%s]"):format(z, dirz, x, dirx, y)
		.."\n"..modAirBoat.translate("ALT.")..(": %02dm"):format(y) --altitude to sea surface
		.."\n"..modAirBoat.translate("PILOT")..(": %03d°"):format(dir_player)
		.."   "..modAirBoat.translate("SHIP")..(": %03d°"):format(dir_airboat)
		.."\n"..modAirBoat.translate("GEAR")..(": %d/4"):format(self.gearshifter)
		.." "..modAirBoat.translate("VEL.")..(": %02d m/s"):format(self.v)
		.."\n"..modAirBoat.translate("FUEL")..(": %02d/%02d (%02d%%)"):format(
			self.fuel, 
			modAirBoat.def.config.fuel_max, 
			(self.fuel/modAirBoat.def.config.fuel_max)*100
		) --Fuel Max = 50L (biofuel) or 50 coal
		.."\n"..modAirBoat.translate("ODO.")..(": %02.1f Km"):format(odometer) --%02.3fKm
		.."\n"..modAirBoat.translate("CLOCK")..": "..modAirBoat.getClockFormated()
end

modAirBoat.hudDraw = function(self, player, isVisible)
	--modAirBoat.hudDraw(self, player, isVisible)
	if type(player)~="nil" and core.is_player(player) then
		local playername = player:get_player_name()
		if type(modAirBoat.hud)=="nil" then
			modAirBoat.hud = { }
		end
		if type(modAirBoat.hud[playername])=="nil" then
			modAirBoat.hud[playername] = { }
		end
		if type(isVisible)~="boolean" then
			isVisible = true
		end
		
		if type(modAirBoat.hud[playername].last_change)~="number"
		   or modAirBoat.hud[playername].last_change + 1 <= os.time()
		   or type(modAirBoat.hud[playername].last_visibled)~="boolean"
		   or modAirBoat.hud[playername].last_visibled ~= isVisible
		then
		   modAirBoat.hud[playername].last_change = os.time()
		   modAirBoat.hud[playername].last_visibled = isVisible
		   
   		--#############################################################################
   		local imgPainel = ""
   		if isVisible then
   			imgPainel = "hud_panel.png^[transformR90^[transformFY" --97x97px
   		end
   		if type(modAirBoat.hud[playername].last_panel)~="string"
   			or modAirBoat.hud[playername].last_panel ~= imgPainel 
   		then
   		   local numHudPanel = 0
   		   if type(modAirBoat.hud[playername].id_panel)=="number" then
   		      numHudPanel = modAirBoat.hud[playername].id_panel
   		      local tblHudPanel = player:hud_get(numHudPanel)
   		      if not type(tblHudPanel)=="table" then
   		         numHudPanel = 0
   		      end
   		   end
   			if type(numHudPanel)=="number" and numHudPanel >= 1 then
   				player:hud_change(numHudPanel, "text", imgPainel)
   			else
   				modAirBoat.hud[playername].id_panel = player:hud_add({
   					hud_elem_type = "image",
   					--position  = {x = 0.70, y = 0},
   					position  = {x = 0, y = 0.5},
   					--offset    = {x = -155, y = 75},
   					offset    = {x = 0, y = 0},
   					text      = imgPainel, --97x97px
   					scale     = { x = 4.10, y = 2.70}, --{ x = 4.10, y = 2.30},
   					--alignment = { x = 0, y = 1 },
   					alignment = { x = 1, y = 0 },
   				})	
   			end
   			modAirBoat.hud[playername].last_panel = imgPainel
   		end
   		--#############################################################################
   		local txtPainel = ""
   		if isVisible then
   			txtPainel = modAirBoat.getHudText(self, player)
   		end
   		if type(modAirBoat.hud[playername].last_text)~="string"
   			or modAirBoat.hud[playername].last_text ~= txtPainel 
   		then
   			local numHudText = 0
   		   if type(modAirBoat.hud[playername].id_text)=="number" then
   		      numHudText = modAirBoat.hud[playername].id_text
   		      local tblHudText = player:hud_get(numHudText)
   		      if not type(tblHudText)=="table" then
   		         numHudText = 0
   		      end
   		   end
   			if type(numHudText)=="number" and numHudText >= 1 then
   				player:hud_change(numHudText, "text", txtPainel)
   			else
   				modAirBoat.hud[playername].id_text = player:hud_add({
   					hud_elem_type	= "text",
   					text				= txtPainel,
   					number			= 0xFFFFFF, --hudColor
   					--position			= {x = 0.70, y = 0},
   					position  = {x = 0, y = 0.5},
   					offset			= {x = 42,	y = 14}, --{x = 42,	y = 7},
   					scale				= {x = 1, y = 1},
   					--alignment		= {x = 0, y = 1},  -- center aligned
   					alignment = { x = 1, y = 0 },
   					--alignment = 0,
   					--direction = 1, --Direction:   0=left-right, 1=right-left, 2=top-bottom, 3=bottom-top
   				})	
   			end
   			modAirBoat.hud[playername].last_text = txtPainel
   		end
   		--#############################################################################
		end --FINAL OF: if type(modAirBoat.hud[playername].last_change)~="number"
	end --FINAL OF: if type(player)~="nil" and player:is_player() then
end

modAirBoat.hudDelete = function(player)
	--modAirBoat.hudDelete(player)
	if type(player)~="nil" and core.is_player(player) then
		local playername = player:get_player_name()
		if type(modAirBoat.hud)=="table" 
		   and type(playername)=="string"
			and type(modAirBoat.hud[playername])~="nil" 
		then
		   if type(modAirBoat.hud[playername].id_panel)=="number" then
   		   local numHudPanel = modAirBoat.hud[playername].id_panel
   		   local tblHudPanel = player:hud_get(numHudPanel)
   			if type(tblHudPanel)=="table" then
   				player:hud_remove(numHudPanel)
   			end
			end
		   if type(modAirBoat.hud[playername].id_text)=="number" then
   		   local numHudText = modAirBoat.hud[playername].id_text
   		   local tblHudText = player:hud_get(numHudText)
   			if type(tblHudText)=="table" then
   				player:hud_remove(numHudText)
   			end
			end
			modAirBoat.hud[playername] = nil
		end
	end
end

--[[  
modAirBoat.delHud = function(self, player)
	if player and player:is_player() then
		local playername = player:get_player_name()
		if modAirBoat.hud and type(modAirBoat.hud[playername])~="nil" then
			if type(modAirBoat.hud[playername].id_panel)~="nil" then
				player:hud_remove(modAirBoat.hud[playername].id_panel)
				modAirBoat.hud[playername].id_panel = nil
			end
			if type(modAirBoat.hud[playername].id_text)~="nil" then
				player:hud_remove(modAirBoat.hud[playername].id_text)
				modAirBoat.hud[playername].id_text = nil
			end
		end
	end
end

modAirBoat.addHud = function(self, clicker)
	local clickername = clicker:get_player_name()
	if type(modAirBoat.hud)=="nil" then
		modAirBoat.hud = { }
	end
	if type(modAirBoat.hud[clickername])=="nil" then
		modAirBoat.hud[clickername] = { }
	end
	
	modAirBoat.delHud(self, clicker)
	
	modAirBoat.hud[clickername].id_panel = clicker:hud_add({
		hud_elem_type = "image",
		--position  = {x = 0.70, y = 0},
		position  = {x = 0, y = 0.5},
		--offset    = {x = -155, y = 75},
		offset    = {x = 0, y = 0},
		text      = "hud_panel.png^[transformR90^[transformFY", --97x97px
		scale     = { x = 4.10, y = 2.30},
		--alignment = { x = 0, y = 1 },
		alignment = { x = 1, y = 0 },
	})
	modAirBoat.hud[clickername].id_text = clicker:hud_add({
		hud_elem_type	= "text",
		text				= modAirBoat.getHudText(self),
		number			= 0xFFFFFF, --hudColor
		--position			= {x = 0.70, y = 0},
		position  = {x = 0, y = 0.5},
		offset			= {x = 42,	y = 7},
		scale				= {x = 1, y = 1},
		--alignment		= {x = 0, y = 1},  -- center aligned
		alignment = { x = 1, y = 0 },
		--alignment = 0,
		--direction = 1, --Direction:   0=left-right, 1=right-left, 2=top-bottom, 3=bottom-top
	})
end

modAirBoat.nowHud = function(self, player)
	if player and player:is_player() then
		local playername = player:get_player_name()
		if modAirBoat.hud and type(modAirBoat.hud[playername])~="nil" and type(modAirBoat.hud[playername].id_text)~="nil" then
			local newText = modAirBoat.getHudText(self)
			if type(modAirBoat.hud[playername].printed)=="nil" or modAirBoat.hud[playername].printed ~= newText  then
				modAirBoat.hud[playername].printed = newText
				player:hud_change(modAirBoat.hud[playername].id_text, "text", newText)
			end			
		end
	end
end
--[[  ]]

modAirBoat.doSoundPassDoor = function(object)
	if object then
		modAirBoat.doSoundPlay(object, "doors_steel_door_open", 16)
		core.after(0.75, function()
			modAirBoat.doSoundPlay(object, "doors_steel_door_close", 16)
		end)
	end
end

modAirBoat.doEnterLikeDriver = function(self, clicker)
	-- Attach
	local clickername = clicker:get_player_name()
	if not self.seized then
		--core.debug(dump(clicker:get_properties()))
		--core.after(0.2, function()
			--default.player_set_animation(clicker, "sit" , 30)
			--default.player_set_animation(clicker, "sit", 30)
			--SINTAXE: player_api.set_animation(player, anim_name, speed)
			--player_api.set_animation(clicker, "sit", 30)
		--end)
		
		self.object:set_properties({infotext = ""})
		modAirBoat.showMainPanel(self, clickername)
		--modAirBoat.hudDraw(self, player, isVisible)
		modAirBoat.hudDraw(self, clicker, true)
		--modAirBoat.addHud(self, clicker)
		
		local attach = clicker:get_attach()
		if attach and attach:get_luaentity() then
			local luaentity = attach:get_luaentity()
			if luaentity.drivername then
				luaentity.drivername = nil
			end
			clicker:set_detach()
		end
		self.drivername = clickername
		
		--modAirBoat.mainChair[clickername] = self.object:get_entity_name()
		--modAirBoat.mainChair[clickername] = self.object:get_luaentity()
		
		default.player_attached[clickername] = true
		default.player_set_animation(clicker, "sit", 30)
		local myYaw = self.object:get_yaw()
		if myYaw then
			clicker:set_look_horizontal(self.object:get_yaw())
		end
		--sintaxe: set_attach(parent[, bone, position, rotation, forced_visible])
		clicker:set_attach(self.object, "",	
			{-- <<< Position
				x=modAirBoat.scale * 0, -- <<< Dislocamento Lateral
				y=modAirBoat.scale * -2.9, 
				z=modAirBoat.scale * 0.75  -- <<< Dislocamento Frontal
			} , 
			{x=0, y=0, z=0} -- <<< Rotation
		)
		clicker:set_properties({
			eye_height = modAirBoat.scale * -0.4, 
			visual_size = {x=2.0/modAirBoat.scale, y=2.0/modAirBoat.scale, z=2.0/modAirBoat.scale},
		})
		clicker:set_eye_offset(
			{ -- <<< Primeira Pessoa
				x=modAirBoat.scale * 0.0, -- <<< Dislocamento Lateral
				y=modAirBoat.scale * 0.0, 
				z=modAirBoat.scale * 0.75  -- <<< Dislocamento Frontal
			}, 
			{ -- <<< Terceira Pessoa
				x=modAirBoat.scale * 0.0, -- <<< Dislocamento Lateral
				y=modAirBoat.scale * 0.0, 
				z=modAirBoat.scale * 0.0  -- <<< Dislocamento Frontal
			}  
		) --O mesma da cadeira de madeira
		--clicker:set_physics_override(0, 0, 0)
		
		--modAirBoat.doSoundPlay(object, soundname, distance, ifHandler, playername, ifLoop)
		--modAirBoat.doSoundPlay(self.object, "doors_steel_door_close")
		modAirBoat.doSoundPassDoor(self.object)
	else
		modAirBoat.doAlertSeizure(self, clickername)
	end
end

modAirBoat.doEnterLikePassenger = function(self, clicker)
	local clickername = clicker:get_player_name()
	if not self.seized then
		if self.passengers.opened_door then
			if type(self.drivername)=="string" and self.drivername~="" and core.get_player_by_name(self.drivername)  then
				if type(self.passengers.name1)=="nil" or self.passengers.name1==""
					or type(self.passengers.name2)=="nil" or self.passengers.name2==""
				then
					if (type(self.passengers.name1)=="string" and self.passengers.name1==clickername)
						or (type(self.passengers.name2)=="string" and self.passengers.name2==clickername)
					then --Check if the player was not already seated.
						modAirBoat.doSoundPlay(self.object, "sfx_panel_fail")
						return
					end --From: if self.passengers.name1~=clickername and self.passengers.name2~=clickername then
					
					if type(self.passengers.name1)=="nil" or self.passengers.name1=="" then
						local attach = clicker:get_attach()
						if attach and attach:get_luaentity() then
							local luaentity = attach:get_luaentity()
							if luaentity.passengers.name1 then
								luaentity.passengers.name1 = nil
							end
							clicker:set_detach()
						end
						self.passengers.name1 = clickername
						default.player_attached[clickername] = true
						default.player_set_animation(clicker, "sit", 30)
						local myYaw = self.object:get_yaw()
						if myYaw then
							clicker:set_look_horizontal(self.object:get_yaw())
						end
						--sintaxe: set_attach(parent[, bone, position, rotation, forced_visible])
						clicker:set_attach(self.object, "",	
							{-- <<< Position
								x=modAirBoat.scale * -0.40, -- <<< Dislocamento Lateral
								y=modAirBoat.scale * -2.9, 
								z=modAirBoat.scale * 0  -- <<< Dislocamento Frontal
							} , 
							{x=0, y=0, z=0} -- <<< Rotation
						)
						clicker:set_eye_offset(
							{ -- <<< Primeira Pessoa
								x=modAirBoat.scale * -1.00, -- <<< Dislocamento Lateral
								y=modAirBoat.scale * 0.0, 
								z=modAirBoat.scale * 0.0  -- <<< Dislocamento Frontal
							}, 
							{ -- <<< Terceira Pessoa
								x=modAirBoat.scale * 0.0, -- <<< Dislocamento Lateral
								y=modAirBoat.scale * 0.0, 
								z=modAirBoat.scale * 0.0  -- <<< Dislocamento Frontal
							}  
						) --O mesma da cadeira de madeira
					elseif type(self.passengers.name2)=="nil" or self.passengers.name2=="" then
						local attach = clicker:get_attach()
						if attach and attach:get_luaentity() then
							local luaentity = attach:get_luaentity()
							if luaentity.passengers.name2 then
								luaentity.passengers.name2 = nil
							end
							clicker:set_detach()
						end
						self.passengers.name2 = clickername
						default.player_attached[clickername] = true
						default.player_set_animation(clicker, "sit", 30)
						local myYaw = self.object:get_yaw()
						if myYaw then
							clicker:set_look_horizontal(self.object:get_yaw())
						end
						--sintaxe: set_attach(parent[, bone, position, rotation, forced_visible])
						clicker:set_attach(self.object, "",	
							{-- <<< Position
								x=modAirBoat.scale * 0.40, -- <<< Dislocamento Lateral
								y=modAirBoat.scale * -2.9, 
								z=modAirBoat.scale * 0  -- <<< Dislocamento Frontal
							} , 
							{x=0, y=0, z=0} -- <<< Rotation
						)
						clicker:set_eye_offset(
							{ -- <<< Primeira Pessoa
								x=modAirBoat.scale * 1.00, -- <<< Dislocamento Lateral
								y=modAirBoat.scale * 0.0, 
								z=modAirBoat.scale * 0.0  -- <<< Dislocamento Frontal
							}, 
							{ -- <<< Terceira Pessoa
								x=modAirBoat.scale * 0.0, -- <<< Dislocamento Lateral
								y=modAirBoat.scale * 0.0, 
								z=modAirBoat.scale * 0.0  -- <<< Dislocamento Frontal
							}  
						) --O mesma da cadeira de madeira
					end
					
					clicker:set_properties({
						eye_height = modAirBoat.scale * -0.4, 
						visual_size = {x=2.0/modAirBoat.scale, y=2.0/modAirBoat.scale, z=2.0/modAirBoat.scale},
					})
				
					modAirBoat.send_message(self, self.drivername, 
						modAirBoat.translate(
							"Passenger '@1' entered your Airboat!"
							, clickername
						), "sfx_refuel_beep"
					)
					--[[
					Só o Motorista pode abrir e fechar as portas! 
					--modAirBoat.doSoundPassDoor(self.object) 
					--]]
				else
					modAirBoat.send_message(self, clickername, 
						modAirBoat.translate(
							"This vehicle does not have space available for more passengers."
						), "sfx_panel_fail"
					)
				end
			else
				modAirBoat.send_message(self, clickername, 
					modAirBoat.translate(
						"It is not possible to enter the vehicle as a passenger before the driver has already entered!"
					), "sfx_panel_fail"
				)
			end
		else
			modAirBoat.send_message(self, clickername, 
				modAirBoat.translate(
					"It is not possible to enter the vehicle as a passenger before the driver opens the door!"
				), "sfx_panel_fail"
			)
		end
	else
		modAirBoat.doAlertSeizure(self, clickername)
	end
end

modAirBoat.doLeavePassenger = function(self, passengername)
	if type(passengername)=="string" and passengername~=""
		and (
			(type(self.passengers.name1)=="string" and self.passengers.name1~="" and passengername == self.passengers.name1)
			or (type(self.passengers.name2)=="string" and self.passengers.name2~="" and passengername == self.passengers.name2)
		) 
	then
		local player = core.get_player_by_name(passengername)
		player:set_detach()
		default.player_attached[passengername] = false
		default.player_set_animation(player, "stand" , 30)
		player:set_properties({
			eye_height = 1.47,
			visual_size = {x=1.0, y=1.0, z=1.0},
		})
		player:set_eye_offset(
			{x=0, y=0, z=0},  -- <<< Primeira Pessoa
			{x=0, y=0, z=0} 	-- <<< Terceira Pessoa
		)
		
		default.player_set_animation(player, "stand", 30)
		if passengername == self.passengers.name1 then
			self.passengers.name1 = ""
		end
		if passengername == self.passengers.name2 then
			self.passengers.name2 = ""
		end
		--[[
		local pos = player:get_pos()
		pos.y = pos.y - (modAirBoat.scale * 0.75)
			player:set_pos(pos)
		--]]
		modAirBoat.doSoundPlay(self.object, "sfx_refuel_beep")
	end	
end

modAirBoat.doLeave = function(self)
	if type(self.drivername)=="string" and self.drivername~="" then
		local player = core.get_player_by_name(self.drivername)
		if player and player:is_player() then
			local playername = self.drivername
			modAirBoat.doEngineStop(self)
			-- Detach
			self.drivername = nil
			self.cruizer_mode = false
			
			modAirBoat.entityFormSpec[playername] = self
			self.object:set_properties({
				--infotext = modAirBoat.translate("AIRBOAT @1", colorName)
				infotext = string.format("%s %s", modAirBoat.getStickerName_by_StickerID(self.sticker), modAirBoat.getColorName_by_ColorID(self.color))				
				.."\n * "..modAirBoat.translate("License Plate: @1", self.license_plate)
				.."\n * "..modAirBoat.translate("Owner: @1", self.owner),
			})
			--modAirBoat.hudDraw(self, player, isVisible)
			modAirBoat.hudDraw(self, player, false)
			--modAirBoat.delHud(self, player)
			
			player:set_detach()
			default.player_attached[playername] = false
			default.player_set_animation(player, "stand" , 30)
			player:set_properties({
				eye_height = 1.47,
				visual_size = {x=1.0, y=1.0, z=1.0},
			})
			player:set_eye_offset(
				{x=0, y=0, z=0}, 
				{x=0, y=0, z=0}
			)
			default.player_set_animation(player, "stand", 30)
			--[[
			local pos = player:get_pos()
			pos.y = pos.y - (modAirBoat.scale * 0.75)
			player:set_pos(pos)
			--]]
			modAirBoat.doLeavePassenger(self, self.passengers.name1)
			core.after(0.5, function()
				modAirBoat.doLeavePassenger(self, self.passengers.name2)
			end)
			modAirBoat.doSoundPassDoor(self.object)				
		end
	end
end

modAirBoat.getDistance = function(p1, p2)
	if type(p1)=="table" and type(p1.x)=="number" and type(p1.y)=="number" and type(p1.z)=="number" then --Avoid BUG
		if type(p2)=="table" and type(p2.x)=="number" and type(p2.y)=="number" and type(p2.z)=="number" then --Avoid BUG
			return math.sqrt(((p1.x-p2.x)^2)+((p1.y-p2.y)^2)+((p1.z-p2.z)^2))
		end
	end
end

modAirBoat.getFloor = function(pos)
	pos.y = pos.y - (modAirBoat.scale*modAirBoat.px*38)
	return core.get_node(pos).name
end

modAirBoat.doCheckLeave = function(self, chairtype)
	if self and type(self.drivername)=="string" and self.drivername~="" then
		local playername = self.drivername
		if type(chairtype)=="string" and chairtype=="passenger1" then
			playername = self.passengers.name1
		elseif type(chairtype)=="string" and chairtype=="passenger2" then
			playername = self.passengers.name2
		end
		local player = core.get_player_by_name(playername)
		
		
		if player and player:is_player() then
			--modAirBoat.debug("[modAirBoat.doCheckLeave()] self.v = "..dump(self.v))
			if self.v <= 0.02  then -- 0.01 m/s = (1cm/s) = 0.01 blocks per second
				local floorPos = self.object:get_pos()
				--modAirBoat.debug("[modAirBoat.doCheckLeave()] nodename = "..dump(nodename))
				if floorPos and modAirBoat.getFloor(floorPos) == "air" then
					--modAirBoat.debug("[modAirBoat.doCheckLeave()] clickername = "..dump(clickername))
					modAirBoat.doSoundPlay(self.object, "sfx_panel_click")
					modAirBoat.entityFormSpec[playername] = self
					core.show_formspec(
						playername,
						"airboat.frmConfirmEject", modAirBoat.getFormSpecs.confirm_eject(self, chairtype)
					)	
				else
					if type(chairtype)=="nil" or (type(chairtype)=="string" and chairtype=="driver") then
						modAirBoat.doLeave(self)
					elseif type(chairtype)=="string" and (chairtype=="passenger1" or chairtype=="passenger2") then
						modAirBoat.doLeavePassenger(self, playername)
					end
				end
			else -- 0.01 m/s = (1cm/s) = 0.01 blocks per second
				modAirBoat.send_message(self, self.drivername, 
					modAirBoat.translate("It is not possible to leave the Airboat before the vehicle stops!")--, sound
				)
			end
		end
	end
end

modAirBoat.doColoring = function(self, player)
	if player and player:is_player() then
		local playername = player:get_player_name()
		local itmNameWield = player:get_wielded_item():get_name()
		for i, row in ipairs(modAirBoat.colors) do
			local colorID = row[1]
			if itmNameWield == "dye:"..colorID then
				--if self.owner == playername then
				if core.get_player_privs(playername).vehicle_mechanic then
					self.color = colorID
					self.object:set_properties({
						textures = {"airboat:nodebox_"..self.color.."_"..self.sticker},
						infotext = string.format("%s %s", modAirBoat.getStickerName_by_StickerID(self.sticker), modAirBoat.getColorName_by_ColorID(self.color))				
							.."\n * "..modAirBoat.translate("License Plate: @1", self.license_plate)
							.."\n * "..modAirBoat.translate("Owner: @1", self.owner),
					})
					modAirBoat.doSoundPlay(self.object, "sfx_refuel_beep", 10)
					if not (creative and creative.is_enabled_for and creative.is_enabled_for(playername)) then
						player:get_inventory():remove_item("main", ItemStack(itmNameWield.." 1"))
					end
				else
					modAirBoat.send_message(self, playername, 
						modAirBoat.translate(
							"Your command has been blocked because you don't have the work (or privilege) of '@1'!"
							,core.colorize("#FF0000", "vehicle_mechanic")
						), "sfx_panel_fail"
					)
				end
				return true
			end
		end
	end
	return false
end

modAirBoat.doStickering = function(self, player)
	if player and player:is_player() then
		local playername = player:get_player_name()
		local itmNameWield = player:get_wielded_item():get_name()
		for i, row in ipairs(modAirBoat.stickers) do
			local stickerID = row[1]
			if itmNameWield == "airboat:sticker_"..stickerID then
				--if self.owner == playername then
				if core.get_player_privs(playername).vehicle_mechanic then
					self.sticker = stickerID
					self.object:set_properties({
						textures = {"airboat:nodebox_"..self.color.."_"..self.sticker},
						infotext = string.format("%s %s", modAirBoat.getStickerName_by_StickerID(self.sticker), modAirBoat.getColorName_by_ColorID(self.color))				
							.."\n * "..modAirBoat.translate("License Plate: @1", self.license_plate)
							.."\n * "..modAirBoat.translate("Owner: @1", self.owner),
					})
					modAirBoat.doSoundPlay(self.object, "sfx_refuel_beep", 10)
					if not (creative and creative.is_enabled_for and creative.is_enabled_for(playername)) then
						player:get_inventory():remove_item("main", ItemStack(itmNameWield.." 1"))
					end
				else
					modAirBoat.send_message(self, playername, 
						modAirBoat.translate(
							"Your command has been blocked because you don't have the work (or privilege) of '@1'!"
							,core.colorize("#FF0000", "vehicle_mechanic")
						), "sfx_panel_fail"
					)
				end
				return true
			end
		end
	end
	return false
end

modAirBoat.doRefuel = function(self, clicker)
	if clicker and clicker:is_player() then
		local clickername = clicker:get_player_name()
		if self.fuel < modAirBoat.def.config.fuel_max then
			local soundPutFuel = "sfx_refuel_beep"
			local doAlertFull = function()
				modAirBoat.send_message(self, clickername, 
					modAirBoat.translate("Storage cannot support this item because it will exceed its maximum capacity!")--, sound
					, "sfx_panel_fail"
				)
			end
			
			local itmNameWield = clicker:get_wielded_item():get_name()
			if 
				core.get_modpath("biofuel") 
				or core.get_modpath("tubelib_addons1") 
			then
				
				if core.get_modpath("tubelib_addons1") then
					if itmNameWield == "tubelib_addons1:biofuel" then
						if self.fuel > modAirBoat.def.config.fuel_max - 0.05 then
							doAlertFull()
							return true
						end
						modAirBoat.doSoundPlay(self.object, soundPutFuel)
						self.fuel = self.fuel + 0.05
						if not (creative and creative.is_enabled_for and creative.is_enabled_for(clickername)) then
							clicker:get_inventory():remove_item("main", ItemStack(itmNameWield.." 1"))
						end
						return true
					end
				end --Final of: if core.get_modpath("tubelib_addons1") then
				
				if core.get_modpath("biofuel") then
					local itmNamePhial = "biofuel:phial_fuel"
					local itmNameBottle = "biofuel:bottle_fuel"
					local itmNameCanister = "biofuel:fuel_can"
					if itmNameWield == itmNamePhial
						or itmNameWield == itmNameBottle
						or itmNameWield == itmNameCanister
					then
						if itmNameWield == itmNamePhial then
							if self.fuel > modAirBoat.def.config.fuel_max - 0.25 then
								doAlertFull()
								return true
							end
							modAirBoat.doSoundPlay(self.object, soundPutFuel)
							self.fuel = self.fuel + 0.25
						end
						if itmNameWield == itmNameBottle then
							if self.fuel > modAirBoat.def.config.fuel_max - 1 then
								doAlertFull()
								return true
							end
							modAirBoat.doSoundPlay(self.object, soundPutFuel)
							self.fuel = self.fuel + 1
						end
						if itmNameWield == itmNameCanister then
							if self.fuel > modAirBoat.def.config.fuel_max - 9 then
								doAlertFull()
								return true
							end
							modAirBoat.doSoundPlay(self.object, soundPutFuel)
							self.fuel = self.fuel + 9
						end
						if not (creative and creative.is_enabled_for and creative.is_enabled_for(clickername)) then
							clicker:get_inventory():remove_item("main", ItemStack(itmNameWield.." 1"))
						end
						return true
					end

				end --Final of: if core.get_modpath("biofuel") then

			else
				if itmNameWield=="default:coal_lump" then
					if self.fuel > modAirBoat.def.config.fuel_max - 2 then
						doAlertFull()
						return true
					end
					modAirBoat.doSoundPlay(self.object, soundPutFuel)
					self.fuel = self.fuel + 2
					if not (creative and creative.is_enabled_for and creative.is_enabled_for(clickername)) then
						clicker:get_inventory():remove_item("main", ItemStack(itmNameWield.." 1"))
					end
					return true
				end
			end
		end --Final of: if self.fuel < modAirBoat.def.config.fuel_max then
	end
end

modAirBoat.showMainPanel = function(self, playername)
	modAirBoat.entityFormSpec[playername] = self
	core.show_formspec(
		playername,
		"airboat.frmMainPanel",
		modAirBoat.getFormSpecs.main_panel(self)
	)
end

modAirBoat.doKeyClick = function(self, clicker)
	if clicker and clicker:is_player() then
		local clickername = clicker:get_player_name()
		local inv = clicker:get_inventory()
		local itmWield = clicker:get_wielded_item()
		local itmName = itmWield:get_name()
		--
		if itmName == "keys:skeleton_key" then --virgin key
			if type(self.owner)=="string" and self.owner == clickername then
			
				--itmWield:take_item()
				if not (creative and creative.is_enabled_for and creative.is_enabled_for(clickername)) then
					inv:remove_item("main", ItemStack(itmName.." 1"))
				end
				local itmSecretKey = ItemStack("keys:key")
				local meta = itmSecretKey:get_meta()
				local secret = core.sha1(self.license_plate..":"..self.owner)
				meta:set_string("secret", secret)
				meta:set_string("description", 
					core.colorize("#00FF00",modAirBoat.translate("AIRBOAT KEY"))
					.."\n * "..modAirBoat.translate("License Plate: @1", core.colorize("#FFFF00",self.license_plate))
					.."\n * "..modAirBoat.translate("Owner: @1", core.colorize("#FFFF00",self.owner))
				)
				
				--local inv = core.get_inventory({type="player", name=clickername})
				
				local leftover = inv:add_item("main", itmSecretKey)
				if leftover:is_empty() then
					--modAirBoat.doSoundPlay(self.object, "sfx_panel_click")
					modAirBoat.send_message(self, clickername, 
						modAirBoat.translate("You have created a ownership transfer key for your Airboat!")
						, "sfx_panel_click"
					)
				else
					core.add_item(self.object:get_pos(), leftover)
					--modAirBoat.send_message(self, playername, message, sound)
					modAirBoat.send_message(self, clickername, 
						modAirBoat.translate("Your item has been placed on the ground because your inventory is full!")
						, "sfx_panel_fail"
					)
				end
			else --from: if type(self.owner)=="string" and self.owner == clickername then
				modAirBoat.doSoundPlay(self.object, "sfx_panel_fail")
			end
			return true --Returning 'true' will prevent the gateway menu from being displayed.
		elseif itmName == "keys:key" then
			if type(self.owner)=="string" and self.owner ~= "" then -- Only use this key if the Aeroboat already has a registered owner.
				if self.owner ~= clickername then
					local meta = itmWield:get_meta()
					local true_secret = core.sha1(self.license_plate..":"..self.owner)
					local key_secret = meta:get_string("secret") or ""
					if key_secret == true_secret then
						self.owner = clickername
						if not (creative and creative.is_enabled_for and creative.is_enabled_for(clickername)) then
							itmWield:take_item(1)
							clicker:set_wielded_item(itmWield)
						end
						modAirBoat.send_message(self, clickername, 
							modAirBoat.translate("Congratulations!  You have become the new registered owner of this Airboat!")
							, "sfx_panel_click"
						)
						return false --Returning 'false' will cause the gateway menu to be displayed.
					else
						modAirBoat.send_message(self, clickername, 
							modAirBoat.translate("This is not a valid key!")
							, "sfx_panel_fail"
						)
					end
				else
					modAirBoat.send_message(self, clickername, 
						modAirBoat.translate("Only use this key if you are not already the registered owner of this Airboat!")
						, "sfx_panel_fail"
					)
				end
				return true--Returning 'true' will prevent the gateway menu from being displayed.
			end
		end
	end
end

modAirBoat.def.on_rightclick = function(self, clicker)
	if not clicker or not clicker:is_player() then
		return
	end
	local clickername = clicker:get_player_name()
	
	if self.owner == "" then
		self.owner = clickername
	end
	
	if modAirBoat.doColoring(self, clicker) 
		or modAirBoat.doStickering(self, clicker)
		or modAirBoat.doKeyClick(self, clicker)
		or modAirBoat.doRefuel(self, clicker)==true
	then
		--modAirBoat.hudDraw(self, player, isVisible)
		modAirBoat.hudDraw(self, clicker, true)
		--modAirBoat.nowHud(self, clicker)
		return
	end
	
	modAirBoat.entityFormSpec[clickername] = self
	if self.drivername and clickername == self.drivername then
		modAirBoat.showMainPanel(self, clickername)
	elseif type(self.passengers.name1)=="string" and (self.passengers.name1==clickername or self.passengers.name2==clickername) then
		if self.passengers.opened_door then
			if self.passengers.name1==clickername then
				modAirBoat.doCheckLeave(self, "passenger1")
			elseif self.passengers.name2==clickername then
				modAirBoat.doCheckLeave(self, "passenger2")
			end
		else
			modAirBoat.send_message(self, clickername, 
				modAirBoat.translate("It is not possible to leave until the driver opens the passenger door.")
				, "sfx_panel_fail"
			)
		end
	else
		modAirBoat.showDoorOutside(self, clickername)
	end
	return false
end

modAirBoat.def.on_activate = function(self, staticdata, dtime_s)
	self.object:set_armor_groups({immortal = 1})
	--core.chat_send_all("staticdata = " .. dump(staticdata))
	if staticdata then
		local memory = core.deserialize(staticdata)
        --core.chat_send_all("modAirBoat.on_activate = " .. dump(memory))
		if type(memory)=="table" then
			self.v = tonumber(memory.vel or 0)
			self.vy = tonumber(memory.vely or 0)
			self.rot = tonumber(memory.rotation or 0)
			self.owner = tostring(memory.owner or "")
			self.color = tostring(memory.color or self.color)
			self.sticker = tostring(memory.sticker or self.sticker)
			self.license_plate = tostring(memory.license_plate or modAirBoat.getRandomLicensePlate(os.time()+math.random(1, 9999)))
			self.odometer = tonumber(memory.odometer or 0)
			self.fuel = tonumber(memory.fuel or self.fuel)
			self.gearshifter = tonumber(memory.gearshifter or self.gearshifter)
			self.seized = (memory.seized==true) or false
			--self.cruizer_mode = tonumber(memory.cruizer_mode or "")
			--modAirBoat.debug("[modAirBoat.def.on_activate()] self.object:get_properties().textures = "..dump(self.object:get_properties().textures))
			--modAirBoat.debug("[modAirBoat.def.on_activate()] self.color = "..dump(self.color))
			self.object:set_properties({
				textures = {"airboat:nodebox_"..self.color.."_"..self.sticker},
			})
			if 
				self.owner ~= "" 
				and self.license_plate ~= "" 
				and tostring(memory.drivername or "") == "" 
			then
				--local colorName = modAirBoat.getColorName_by_ColorID(self.color)
				self.object:set_properties({
					--infotext = modAirBoat.translate("AIRBOAT @1", colorName)--..self.color
					infotext = string.format("%s %s", modAirBoat.getStickerName_by_StickerID(self.sticker), modAirBoat.getColorName_by_ColorID(self.color))				
						.."\n * "..modAirBoat.translate("License Plate: @1", self.license_plate)
						.."\n * "..modAirBoat.translate("Owner: @1", self.owner),
				})
			end
			
			--[[			
			self.nametag = tostring(memory.nametag or "")
			if self.nametag ~= "" then
				self.object:set_properties({
					nametag = self.nametag,
					nametag_color = modAirBoat.def.config.nametag_color,
				})
			end
			--]]
			
			local drivername = tostring(memory.drivername or "")
			if drivername ~= "" then
				local driver = core.get_player_by_name(drivername)
				if type(driver)~="nil" and driver:is_player() then
					core.after(0.1, function()
						modAirBoat.doEnterLikeDriver(self, driver)
					end)
				end
			end
		end
	end
end

modAirBoat.def.get_staticdata = function(self)
	--return tostring(v)
	
	local mystaticdata = core.serialize({
		vel = self.v,
		vely = self.vy,
		rotation = self.rot,
		owner = self.owner or "",
		color = self.color,
		sticker = self.sticker,
		license_plate = self.license_plate or modAirBoat.getRandomLicensePlate(os.time()+math.random(1, 9999)),
		drivername = self.drivername or "",
		odometer = tonumber(self.odometer),
		fuel = tonumber(self.fuel),
		gearshifter = tonumber(self.gearshifter),
		seized = self.seized or false
		--cruizer_mode = self.cruizer_mode,
	})
    --core.debug("modAirBoat.get_staticdata = " .. dump(mystaticdata))
    return mystaticdata
end

modAirBoat.doAlertSeizure = function(self, playername)
	local msg = modAirBoat.translate("This Airboat has been seized by the police!")
	local soundname = "sfx_panel_fail"
	if not self.seized then
		msg = modAirBoat.translate("You undid seizure of the Aeroboat!")
		soundname = "sfx_panel_click"
	end
	--modAirBoat.doSoundPlay(self.object, "sfx_panel_click")
	modAirBoat.send_message(self, playername, msg, soundname)
end

modAirBoat.doEncapsulate = function(self, player)
	if player and player:is_player() and not self.removed then
		local playername = player:get_player_name()
		if not self.seized then
			if 
				(modAirBoat.getEncapsulater() == "owner" and (self.owner=="" or self.owner == playername))
				or (type(core.get_player_privs(playername)[modAirBoat.getEncapsulater()])=="boolean" and core.get_player_privs(playername)[modAirBoat.getEncapsulater()]==true)
			then
				if not self.drivername then
					-- Move to inventory
					self.removed = true
					local idItem = "airboat:nodebox_"..self.color.."_"..self.sticker
					
					local inv = player:get_inventory()
					if not (creative and creative.is_enabled_for and creative.is_enabled_for(playername))	or not inv:contains_item("main", idItem) then
						
						local new_stack = ItemStack(idItem)
						
						--local colorName = modAirBoat.getColorName_by_ColorID(self.color)
						local tmpDatabase = {
								owner = self.owner,
								license_plate = self.license_plate,
								color = self.color,
								sticker = self.sticker,
								odometer = self.odometer,
								nametag = self.nametag,
								fuel = self.fuel,
								gearshifter = self.gearshifter,
								--description = core.colorize("#00FF00",modAirBoat.translate("AIRBOAT @1", colorName))
								description = string.format("%s %s", modAirBoat.getStickerName_by_StickerID(self.sticker), modAirBoat.getColorName_by_ColorID(self.color))				
								.."\n * "..modAirBoat.translate("License Plate: @1", core.colorize("#FFFF00",self.license_plate))
								.."\n * "..modAirBoat.translate("Owner: @1", core.colorize("#FFFF00",self.owner))
								.."\n * "..modAirBoat.translate("Odometer: @1", core.colorize("#FFFF00",string.format("%02.1f Km", self.odometer/1000)))
							} 
						new_stack:get_meta():from_table({fields = tmpDatabase})
						local leftover = inv:add_item("main", new_stack)
						if not leftover:is_empty() then
							core.add_item(self.object:get_pos(), leftover)
							--modAirBoat.send_message(self, playername, message, sound)
							modAirBoat.send_message(self, playername, 
								modAirBoat.translate("Your item has been placed on the ground because your inventory is full!")
								, "sfx_panel_fail"
							)
						else
							modAirBoat.doSoundPlay(self.object, "sfx_panel_click")
						end
					end
					core.after(0.1, function()
						self.object:remove()
					end)
				else --From: if not self.drivername then
					modAirBoat.doSoundPlay(self.object, "sfx_panel_click")
					--modAirBoat.hudDraw(self, player, isVisible)
					modAirBoat.hudDraw(self, player, true)
					--modAirBoat.addHud(self, player) --as vezes o hud some. dar uma pancada resolve!
				end
			else 
				--modAirBoat.doSoundPlay(self.object, "sfx_panel_click")
				modAirBoat.send_message(self, playername, 
					modAirBoat.translate(
						"Your command has been blocked because you don't have the work (or privilege) of '@1'!"
						, core.colorize("#FF0000", modAirBoat.getEncapsulater())
					), "sfx_panel_fail"
				)
			end
		else --from: if not self.seized then
			modAirBoat.doAlertSeizure(self, playername)
		end
	end --from: if player and player:is_player() and not self.removed then
end

modAirBoat.def.on_punch = function(self, puncher)
	if not puncher or not puncher:is_player() then
		return
	end
	local punchername = puncher:get_player_name()
	if self.drivername == "" or self.drivername ~= punchername then
		return
	end
	--modAirBoat.hudDraw(self, player, isVisible)
	modAirBoat.hudDraw(self, puncher, true)
	--modAirBoat.addHud(self, puncher)
end

modAirBoat.calcOdometer = function(self)
	if type(self.odometer)=="number" then		
		if self.old_pos ~= self.object:get_pos() then
			local dist = modAirBoat.getDistance(self.old_pos, self.object:get_pos())
			if type(dist)=="number" and dist > 0 then
				self.odometer = tonumber(self.odometer) + dist
			end
			self.old_pos = self.object:get_pos()
		end
	end
end

modAirBoat.getMaxVel = function(self)
	if self.gearshifter == 1 then
		return 2
	elseif self.gearshifter == 2 then
		return 5
	elseif self.gearshifter == 3 then
		return 10
	else
		return 15
	end
end

modAirBoat.doGearboxLevelUp = function(self)
	if self.gearshifter == 1 and self.v >= 1.75 then
		modAirBoat.doSoundPlay(self.object, "sfx_gearbox_levelup")
		self.gearshifter = 2
	elseif self.gearshifter == 2 and self.v >= 4.75 then
		modAirBoat.doSoundPlay(self.object, "sfx_gearbox_levelup")
		self.gearshifter = 3
	elseif self.gearshifter == 3 and self.v >= 9.75 then
		modAirBoat.doSoundPlay(self.object, "sfx_gearbox_levelup")
		self.gearshifter = 4
	else
		modAirBoat.doSoundPlay(self.object, "sfx_panel_fail")
	end
	--modAirBoat.controls[self.drivername].jump = true
end

modAirBoat.doGearboxLevelDown = function(self)
	--modAirBoat.debug("self.v = "..self.v)
	if self.gearshifter == 4 then
		modAirBoat.doSoundPlay(self.object, "sfx_gearbox_leveldown")
		self.gearshifter = 3
	elseif self.gearshifter == 3 and math.floor(self.v) <= 11.3 then
		modAirBoat.doSoundPlay(self.object, "sfx_gearbox_leveldown")
		self.gearshifter = 2
	elseif self.gearshifter == 2 and math.floor(self.v) <= 6.3 then
		modAirBoat.doSoundPlay(self.object, "sfx_gearbox_leveldown")
		self.gearshifter = 1
	else
		modAirBoat.doSoundPlay(self.object, "sfx_panel_fail")
	end
	--modAirBoat.controls[self.drivername].sneak = true
end

modAirBoat.def.on_step = function(self, dtime)
	self.v = get_v(self.object:get_velocity()) * get_sign(self.v)
	self.vy = self.object:get_velocity().y

	local new_acce = {x = 0, y = 0, z = 0}
	local p = self.object:get_pos()
	p.y = p.y - (0.74*modAirBoat.scale)
	local defnode = core.registered_nodes[core.get_node(p).name]
	
	-- Controls
	if type(self.drivername)=="string" and self.drivername~="" then
		local objDriver = core.get_player_by_name(self.drivername)
		if objDriver then
			modAirBoat.calcOdometer(self)
			--modAirBoat.hudDraw(self, player, isVisible)
			modAirBoat.hudDraw(self, objDriver, true)
			--modAirBoat.nowHud(self, objDriver)
			if self.started then
				
				if self.fuel <= 0 then
					modAirBoat.send_message(self, self.drivername, 
						modAirBoat.translate(
							"The vehicle shut down due to running out of fuel."
						)--, sound
					)
					modAirBoat.doEngineStop(self)
				end
				
				local ctrl = objDriver:get_player_control()
				if ctrl.aux1 
					and not ctrl.up and not ctrl.down
					and not ctrl.left and not ctrl.right
					and not ctrl.sneak and not ctrl.jump
				then
					self.object:set_yaw(objDriver:get_look_horizontal())
				end
				
				
				if (ctrl.up and ctrl.down) and not ctrl.aux1 then
					--Mesmo que [RMB] no PC, ou [Double Click] nos Dispositivos Móveis.
					modAirBoat.def.on_rightclick(self, objDriver)
				elseif (ctrl.up and not ctrl.down) and ctrl.aux1 then
					if not self.cruizer_mode then
						self.cruizer_mode = true
						modAirBoat.send_message(self, self.drivername, 
							modAirBoat.translate(
								"Cruise Mode: @1"
								,core.colorize("#8888FF", modAirBoat.translate("ON"))
							)--, sound
						)
					end
				elseif ctrl.down and not ctrl.up then
					if ctrl.aux1 then
						self.v = 0
						self.vy = 0
					else
						self.v = self.v - modAirBoat.def.config.vel_walk
						self.fuel = self.fuel - modAirBoat.def.config.fuel_consumption_walk
					end
					if self.cruizer_mode then
						self.cruizer_mode = false
						--core.chat_send_player(self.drivername,	"[airboat] Cruise Mode OFF")
						modAirBoat.send_message(self, self.drivername, 
							modAirBoat.translate(
								"Cruise Mode: @1"
								,core.colorize("#FF0000", modAirBoat.translate("OFF"))
							)--, sound
						)
					end
				elseif (ctrl.up and not ctrl.down) or self.cruizer_mode then
					self.v = self.v + modAirBoat.def.config.vel_walk
					self.fuel = self.fuel - modAirBoat.def.config.fuel_consumption_walk
				end
				
				if not ctrl.aux1 then
					if ctrl.left then
						self.rot = self.rot + modAirBoat.def.config.vel_rot
						self.fuel = self.fuel - modAirBoat.def.config.fuel_consumption_walk
					elseif ctrl.right then
						self.rot = self.rot - modAirBoat.def.config.vel_rot
						self.fuel = self.fuel - modAirBoat.def.config.fuel_consumption_walk
					end
					--self.object:set_yaw(self.object:get_yaw() + (1 + dtime) * self.rot)
				else
					local vel = get_velocity(self.v, self.object:get_yaw(), self.vy)
					if ctrl.left then
						vel.x = vel.x + (math.cos(self.object:get_yaw()-math.pi)*modAirBoat.def.config.vel_side)
						vel.z = vel.z + (math.sin(self.object:get_yaw()+math.pi)*modAirBoat.def.config.vel_side)
						self.fuel = self.fuel - modAirBoat.def.config.fuel_consumption_walk		
					elseif ctrl.right then
						vel.x = vel.x - (math.cos(self.object:get_yaw()-math.pi)*modAirBoat.def.config.vel_side)
						vel.z = vel.z - (math.sin(self.object:get_yaw()+math.pi)*modAirBoat.def.config.vel_side)
						self.fuel = self.fuel - modAirBoat.def.config.fuel_consumption_walk
					end
					self.object:set_velocity(vel)
				end			
				
				if type(modAirBoat.controls)=="nil" or type(modAirBoat.controls[self.drivername])=="nil" then
					modAirBoat.controls = { 
						[self.drivername] = {
							jump = false,
							sneak = false,
						} 
					}
				end
				if not ctrl.aux1 then
					if ctrl.jump and not ctrl.sneak then
						self.vy = self.vy + modAirBoat.def.config.vel_fly
						self.fuel = self.fuel - modAirBoat.def.config.fuel_consumption_walk
					elseif not ctrl.jump and ctrl.sneak then
						self.vy = self.vy - modAirBoat.def.config.vel_fly
					elseif ctrl.jump and ctrl.sneak then
						--self.vy = 0
						--Desativa Futuro Auto-Piloto
					end
				else
					
					if ctrl.jump and not ctrl.sneak then
						--up gearshifter
						if not modAirBoat.controls[self.drivername].jump then
							modAirBoat.doGearboxLevelUp(self)
						end
					elseif not ctrl.jump and ctrl.sneak then
						--down gearshifter
						if not modAirBoat.controls[self.drivername].sneak then
							modAirBoat.doGearboxLevelDown(self)
						end
					elseif ctrl.jump and ctrl.sneak and not ctrl.aux1 then
						--  [A Definir]
					end
				end
				
				modAirBoat.controls[self.drivername].jump = ctrl.jump
				modAirBoat.controls[self.drivername].sneak = ctrl.sneak
			
				self.fuel = self.fuel - modAirBoat.def.config.fuel_consumption_fly
			end --from: if self.started then
		else --ELSE OF: if objDriver then
			self.cruizer_mode = false
			self.started = false
			--core.log("warning", "[airboat] drivername left server while driving. This may cause some 'Pushing ObjectRef to removed/deactivated object' warnings.")
		end --from: if objDriver then
	else --ELSE OF: if objDriver then
		if type(self.owner)=="string" and self.owner ~= "" then
			local objOwner = core.get_player_by_name(self.owner)
			if type(objOwner)~="nil" and objOwner:is_player() then
				--modAirBoat.hudDraw(self, player, isVisible)
				modAirBoat.hudDraw(self, objOwner, false)
			end
		end
	end --form if type(self.drivername)=="string" and self.drivername~="" then

	if not self.started then
		self.v = 0
		if not type(defnode)=="table" or not (defnode.liquidtype == "source" or defnode.liquidtype == "flowing") then
			self.vy = self.vy - modAirBoat.def.config.vel_fly
		end
	end

	if type(self.fuel)~="number" or self.fuel < 0 then
		self.fuel = 0
	end

	-- Early return for stationary vehicle
	if self.v == 0 and self.rot == 0 and self.vy == 0 then
		self.object:set_pos(self.object:get_pos())
		return
	end

	-- Reduction and limiting of linear speed
	local s = get_sign(self.v)
	self.v = self.v - 0.02 * s
	if s ~= get_sign(self.v) then
		self.v = 0
	end
	if math.abs(self.v) > modAirBoat.getMaxVel(self) + 1 then
		self.v = (modAirBoat.getMaxVel(self) + 1) * get_sign(self.v)
	end

	-- Reduction and limiting of rotation
	local sr = get_sign(self.rot)
	self.rot = self.rot - 0.0003 * sr
	if sr ~= get_sign(self.rot) then
		self.rot = 0
	end
	if math.abs(self.rot) > 0.015 then
		self.rot = 0.015 * get_sign(self.rot)
	end

	local sy = get_sign(self.vy) --return: -1, 0,ou +1
	self.vy = self.vy - 0.03 * sy --Slow down when going up or down.
	if math.abs(self.vy) > 4 then self.vy = 4 * get_sign(self.vy) end --Define max speed to fly and to fall
	if defnode and (
	   defnode.liquidtype == "source" 
	   or defnode.liquidtype == "flowing"
	) then
	   -- Bouyancy in liquids
		new_acce = {x = 0, y = 4, z = 0}
	elseif get_sign(self.vy)>0 
	   and math.floor(self.object:get_pos().y - modAirBoat.scale) >= modAirBoat.max_flight_height 
	then 
	   self.vy = self.vy - modAirBoat.def.config.vel_fly --It will stop increasing altitude if it reaches the maximum allowed height.
	end 
	if sy ~= get_sign(self.vy) then self.vy = 0 end --It will stop the movement if the deceleration reverses the vector.

	self.object:set_pos(self.object:get_pos())
	self.object:set_velocity(get_velocity(self.v, self.object:get_yaw(), self.vy))
	self.object:set_acceleration(new_acce)
	self.object:set_yaw(self.object:get_yaw() + (1 + dtime) * self.rot)
end

modAirBoat.doEngineStart = function(self)
	if type(self.drivername)=="string" and self.drivername ~="" then
		modAirBoat.entityFormSpec[self.drivername] = self
		--modAirBoat.doSoundPlay(object, soundname, distance, ifHandler, playername, ifLoop)
		modAirBoat.doSoundPlay(self.object, "sfx_engine_start_060p", 16)
		if type(self.fuel)=="number" and self.fuel > 0 then
			if type(self.drivername)=="string" and self.drivername~="" then
				core.after(0.5, function()
					modAirBoat.doSoundPlay(self.object, "sfx_engine_continuo_060p", 16, true, self.drivername, true)
				end)
				self.started = true
				--modAirBoat.hudDraw(self, player, isVisible)
				modAirBoat.hudDraw(self, core.get_player_by_name(self.drivername), true)
				--modAirBoat.nowHud(self, core.get_player_by_name(self.drivername))
			end
		else
			modAirBoat.send_message(self.object, self.drivername, 
				modAirBoat.translate(
					"The engine failed to start due to lack of fuel!"
				)--, sound
			)
		end
	end
end

modAirBoat.doEngineStop = function(self)
	self.started = false
	if type(self.drivername)=="string" and self.drivername~="" then
		modAirBoat.entityFormSpec[self.drivername] = self
		modAirBoat.doSoundStop(self.drivername, "sfx_engine_continuo_060p")
		--modAirBoat.hudDraw(self, player, isVisible)
		modAirBoat.hudDraw(self, core.get_player_by_name(self.drivername), true)
		--modAirBoat.nowHud(self, core.get_player_by_name(self.drivername))
	end
end

modAirBoat.getFileContent = function(myFile)
	local handler = io.open(modAirBoat.modpath.."/"..myFile , "rb")
	if handler then
		local content = handler:read("*all")
		handler:close()
		return content
	end
end

modAirBoat.showDoorOutside = function(self, playername)
	core.show_formspec(
		playername,
		"airboat.frmDoorSideOut",
		modAirBoat.getFormSpecs.door_sideout(self, playername)
	)	
end

modAirBoat.on_player_receive_fields = function(sender, formname, fields)
	--modAirBoat.on_player_receive_fields(sender, formname, fields)
	if sender and sender:is_player() then
		local sendername = sender:get_player_name()
		if sendername and sendername~="" and type(modAirBoat.entityFormSpec[sendername]) ~= "nil" then
			local self = modAirBoat.entityFormSpec[sendername]
			--modAirBoat.debug("[airboat -> on_player_receive_fields] formname = '"..dump(formname).."' | fields = "..dump(fields))
			if fields then
				if fields.btnMainPanel then
					modAirBoat.doSoundPlay(self.object, "sfx_panel_click")
					modAirBoat.showMainPanel(self, self.drivername)
				elseif formname == "airboat.frmDoorSideOut" then
					if fields.btnEnterLikeDriver then
						if self.owner == "" or self.owner == sendername or core.get_player_privs(sendername).valet_parking then
							modAirBoat.doEnterLikeDriver(self, sender)
						else
							modAirBoat.send_message(self, sendername, 
								modAirBoat.translate(
									"This Airboat belongs to '@1'!"
									,core.colorize("#FFFF00", self.owner)
								), "sfx_panel_fail"
							)
						end
					elseif fields.btnEnterLikePassenger then
						modAirBoat.doEnterLikePassenger(self, sender)
					elseif fields.btnSeizure then
						if core.get_player_privs(sendername).police_officer then
							modAirBoat.doSoundPlay(self.object, "sfx_panel_click")
							self.seized = not self.seized
							modAirBoat.doAlertSeizure(self, sendername)
							if not self.seized then
								modAirBoat.showDoorOutside(self, sendername)
							end
						else
							--modAirBoat.doSoundPlay(self.object, "sfx_panel_fail")
							modAirBoat.send_message(self, sendername, 
								modAirBoat.translate(
									"Your command has been blocked because you don't have the work (or privilege) of '@1'!"
									,core.colorize("#FF0000", "police_officer")
								), "sfx_panel_fail"
							)
						end
					elseif fields.btnEncapsulate then
						modAirBoat.doEncapsulate(self, sender)
					end
				elseif formname == "airboat.frmMainPanel" and type(self.drivername)=="string" and self.drivername~="" then
					if fields.btnUserManual then
						modAirBoat.doSoundPlay(self.object, "sfx_panel_click")
						core.show_formspec(
							self.drivername,
							"airboat.frmUserManual",
							modAirBoat.getFormSpecs.user_manual(self)
						)
					elseif fields.btnTongleDoor then
						self.passengers.opened_door = not self.passengers.opened_door
						--modAirBoat.doSoundPlay(self.object, "sfx_panel_click")
						if self.passengers.opened_door then
							modAirBoat.doSoundPlay(self.object, "doors_steel_door_open")
						else
							modAirBoat.doSoundPlay(self.object, "doors_steel_door_close")
						end
						modAirBoat.showMainPanel(self, self.drivername)
					elseif fields.btnEngineStart then
						modAirBoat.doEngineStart(self, self.drivername)
					elseif fields.btnEngineStop then
						modAirBoat.doEngineStop(self)
					elseif fields.btnSoundHorn then
						if modAirBoat.horn_enabled then
							--modAirBoat.debug("os.time() = "..os.time().." | self.lastPlayHorn = "..dump(self.lastPlayHorn))
							if type(self.lastPlayHorn)=="nil" or os.time() >= self.lastPlayHorn + tonumber(modAirBoat.horn_interval) then
								self.lastPlayHorn = os.time()
								modAirBoat.doSoundPlay(self.object, "sfx_horn_truck", 32)
							end
						else
							modAirBoat.doSoundPlay(self.object, "sfx_panel_fail")
						end
					elseif fields.btnCheckLeave then
						--modAirBoat.debug("[airboat -> on_player_receive_fields] fields.btnCheckLeave")
						modAirBoat.doCheckLeave(self)
					elseif fields.btnTongleCrizerMode then
						self.cruizer_mode = not self.cruizer_mode
						local label = modAirBoat.translate("OFF")
						local color = "#FF0000"
						if self.cruizer_mode then
							label = modAirBoat.translate("ON")
							color = "#8888FF"
						end
						modAirBoat.doSoundPlay(self.object, "sfx_panel_click")
						modAirBoat.send_message(self, self.drivername, 
							modAirBoat.translate(
								"Cruise Mode: @1"
								,core.colorize(color, label)
							)--, sound
						)
						modAirBoat.showMainPanel(self, self.drivername)
					elseif fields.btnGearboxLevelUp then
						modAirBoat.doGearboxLevelUp(self)
						modAirBoat.showMainPanel(self, self.drivername)
					elseif fields.btnGearboxLevelDown then
						modAirBoat.doGearboxLevelDown(self)
						modAirBoat.showMainPanel(self, self.drivername)
					end
				elseif formname == "airboat.frmUserManual" then
					if fields.btnBack then
						modAirBoat.doSoundPlay(self.object, "sfx_panel_click")
						modAirBoat.showMainPanel(self, self.drivername)
					end
				elseif formname == "airboat.frmConfirmEject" then
					if fields.btnEjectOk then
						--modAirBoat.send_message(self, sendername, "fields.btnEjectOk="..fields.btnEjectOk)
						modAirBoat.doLeave(self)
					elseif fields.btnEjectP1Ok then
						modAirBoat.doLeavePassenger(self, self.passengers.name1)
					elseif fields.btnEjectP2Ok then
						modAirBoat.doLeavePassenger(self, self.passengers.name2)
					end
				end
			end
		end
	end
end

modAirBoat.get_entity = function()
	return "airboat:airboat", modAirBoat.def
end

modAirBoat.getColoring = function(color)
	if type(color)=="string" and color~="" then
		--[colorize:<color>:<ratio>
		--return "^[colorize:"..color..":128"
		
		--[multiply:<color>
		return "^[multiply:"..color
	else
		return ""
	end
end

modAirBoat.getSticker = function(stickername)
	local selTexture = ""
	for i, row in ipairs(modAirBoat.stickers) do
		local stickerID = row[1]
		local stickerTexture = row[2]
		local stickerName = row[3]
		if stickerID == stickername then
			selTexture = stickerTexture
			break
		end
	end
	if selTexture == "" then
		selTexture = modAirBoat.stickers[1][2] -- <== text_airboat_sticker.default.png
	end
	return "^"..selTexture
end

modAirBoat.getTexture = function(color, stickername)
	local tiles = { -- Top, base, right, left, front, back
		"text_airboat_top.png"..modAirBoat.getColoring(color), --top
		"text_airboat_base.png"..modAirBoat.getColoring(color), --base
		"text_airboat_right.png"..modAirBoat.getColoring(color)..modAirBoat.getSticker(stickername), --right
		"text_airboat_left.png"..modAirBoat.getColoring(color)..modAirBoat.getSticker(stickername), --left
		"text_airboat_front.png"..modAirBoat.getColoring(color), --from
		--"text_airboat_back.png"..modAirBoat.getColoring(color), --back
		{ --back
			name="text_airboat_back.png"..modAirBoat.getColoring(color), --48x144
			animation={
				type="vertical_frames",
				aspect_w = 48,	-- <= (01 ui)
				aspect_h = 48,	-- <= 144/3 = 48 (05ui)
				length = 0.333			-- <= Tempo de duracao em segundos!!!
			}
		}
	}
	return tiles
end

modAirBoat.node_can_dig = function(pos, player, colorID, stickerID)
	local playername = player:get_player_name()
	local meta = core.get_meta(pos)
	local owner = meta:get_string("owner") or playername
	if not owner or owner == "" or owner == playername
		--or core.get_player_privs(namePlayer).server
		--or core.get_player_privs(namePlayer).mayor
		or (
			core.get_modpath("tradelands")
			and modTradeLands~=nil
			and modTradeLands.getOwnerName(pos)~=""
			and modTradeLands.canInteract(pos, playername)
		)
		or (
			core.get_modpath("landrush")
			and landrush~=nil
			and landrush.get_owner(pos)~=nil
			and landrush.can_interact(pos, playername)
		)
	then
		local license_plate = meta:get_string("license_plate") or modAirBoat.getRandomLicensePlate(os.time()+math.random(1, 9999))
		local odometer = meta:get_string("odometer") or 0
		local nametag = meta:get_string("nametag") or ""
		local fuel = meta:get_string("fuel") or 0
		local gearshifter = meta:get_string("gearshifter") or 1
		local color = meta:get_string("color") or colorID
		local sticker = meta:get_string("sticker") or stickerID
		local idItem = "airboat:nodebox_"..color.."_"..sticker
	
	
		local inv = player:get_inventory()
		local new_stack = ItemStack(idItem)
		--local colorName = modAirBoat.getColorName_by_ColorID(color)
		local tmpDatabase = {
				owner = owner,
				license_plate = license_plate,
				odometer = odometer,
				nametag = nametag,
				fuel = fuel,
				gearshifter = gearshifter,
				color = color,
				--description = core.colorize("#00FF00",modAirBoat.translate("AIRBOAT @1", colorName))
				description = string.format("%s %s", modAirBoat.getStickerName_by_StickerID(sticker), modAirBoat.getColorName_by_ColorID(color))
				.."\n * "..modAirBoat.translate("License Plate: @1", core.colorize("#FFFF00", license_plate))
				.."\n * "..modAirBoat.translate("Owner: @1", core.colorize("#FFFF00", owner))
				.."\n * "..modAirBoat.translate("Odometer: @1", core.colorize("#FFFF00",string.format("%02.1f Km", odometer/1000)))
			} 
		new_stack:get_meta():from_table({fields = tmpDatabase})
		local leftover = inv:add_item("main", new_stack)
		if not leftover:is_empty() then
			core.add_item(player:get_pos(), leftover)
			--modAirBoat.send_message(self, playername, message, sound)
			modAirBoat.send_message(nil, playername, 
				modAirBoat.translate("Your item has been placed on the ground because your inventory is full!")
				, "sfx_panel_fail"
			)
		end
		core.remove_node(pos)
	end
	
	return false
end

modAirBoat.node_on_place = function(itemstack, placer, pointed_thing, colorID, stickerID)
	local above = pointed_thing.above
	local under = pointed_thing.under
	local node = core.get_node(under)
	local udef = core.registered_nodes[node.name]

	if udef and udef.on_rightclick and
			not (placer and placer:is_player() and
			placer:get_player_control().sneak) 
	then
		return udef.on_rightclick(under, node, placer, itemstack,
			pointed_thing) or itemstack
	end

	if pointed_thing.type ~= "node" then
		return itemstack
	end
	
	local placername = placer:get_player_name()
	
	local data = itemstack:get_meta():to_table().fields
	--modAirBoat.debug("[airboat][modAirBoat.node_on_place] data = "..dump(data))
	local staticdata = core.serialize(data)
	if placer:get_player_control().aux1 then
		pointed_thing.under.y = pointed_thing.under.y + (modAirBoat.scale)
		
		local obj = core.add_entity(
			pointed_thing.under,
			"airboat:airboat"
			, staticdata
		)	
		
		--modAirBoat.debug("[airboat][modAirBoat.node_on_place] staticdata = "..dump(staticdata))
		if obj then
			local license_plate = obj:get_luaentity().license_plate or modAirBoat.getRandomLicensePlate(os.time()+math.random(1, 9999))
			local owner = obj:get_luaentity().owner or placername
			--modAirBoat.debug("[modAirBoat.node_on_place()] obj = "..dump(obj))
			--modAirBoat.debug("[modAirBoat.node_on_place()] obj:get_properties() = "..dump(obj:get_properties()))
			--modAirBoat.debug("[modAirBoat.node_on_place()] obj:get_luaentity().object = "..dump(obj:get_luaentity().object))
			--modAirBoat.debug("[modAirBoat.node_on_place()] obj:get_luaentity() = "..dump(obj:get_luaentity()))
			--modAirBoat.debug("[modAirBoat.node_on_place()] obj:get_properties().textures = "..dump(obj:get_properties().textures))
			obj:set_yaw(placer:get_look_horizontal())
			obj:get_luaentity().owner = owner
			obj:get_luaentity().color = colorID
			obj:get_luaentity().sticker = stickerID
			obj:get_luaentity().license_plate = license_plate
		   --obj:get_luaentity().fuel = tonumber(data.fuel) or 0
			
			--modAirBoat.debug("data = "..dump(data))
			--modAirBoat.debug("data.fuel = "..data.fuel)
			--obj:get_luaentity().fuel = data.fuel or modAirBoat.def.config.fuel_max
			
			obj:set_properties({
				infotext = string.format("%s %s", modAirBoat.getStickerName_by_StickerID(stickerID), modAirBoat.getColorName_by_ColorID(colorID))
					.."\n * "..modAirBoat.translate("License Plate: @1", license_plate)
					.."\n * "..modAirBoat.translate("Owner: @1", owner),
				textures = {"airboat:nodebox_"..colorID.."_"..stickerID},
				nametag_color = modAirBoat.def.config.nametag_color,
			})
		end
		--modAirBoat.debug("[modAirBoat.node_on_place()] obj:get_properties().textures = "..dump(obj:get_properties().textures))
	else --from: if not placer:get_player_control().aux1 then
		
		--modAirBoat.debug("[modAirBoat.node_on_place()] placer:get_look_dir() = "..dump(placer:get_look_dir()))
		local facedir = core.dir_to_facedir(placer:get_look_dir())
		facedir = (facedir + 3) % 4 --Rotste 90° to left
		--modAirBoat.debug("[modAirBoat.node_on_place()] facedir= "..dump(facedir))
		core.set_node(above, {
			name = "airboat:nodebox_"..colorID.."_"..stickerID,
			param2 = facedir,
		})
		local license_plate = data.license_plate or modAirBoat.getRandomLicensePlate(os.time()+math.random(1, 9999))
		local owner = data.owner or placername
		local odometer = data.odometer or 0
		local nametag = data.nametag or ""
		local fuel = data.fuel or 0
		local gearshifter = data.gearshifter or 1
		local meta = core.get_meta(above)
		meta:set_string("license_plate", license_plate)
		meta:set_string("owner", owner)
		meta:set_string("odometer", odometer)
		meta:set_string("nametag", nametag)
		meta:set_string("fuel", fuel)
		meta:set_string("gearshifter", gearshifter)
		meta:set_string("color", colorID)
		meta:set_string("sticker", stickerID)
		meta:set_string("infotext", 
			--modAirBoat.translate("AIRBOAT @1", modAirBoat.getColorName_by_ColorID(colorID))
			string.format("%s %s", modAirBoat.getStickerName_by_StickerID(stickerID), modAirBoat.getColorName_by_ColorID(colorID))
			.."\n * "..modAirBoat.translate("License Plate: @1", license_plate)
			.."\n * "..modAirBoat.translate("Owner: @1", owner)
			.."\n * "..modAirBoat.translate(
				"Odometer: @1", 
				string.format("%02.1f Km", odometer/1000)
			)
		)
	end--from: if not placer:get_player_control().aux1 then
	--[[  
	if not (creative and creative.is_enabled_for and creative.is_enabled_for(placername)) then
		itemstack:take_item()
	end
	--]]
	itemstack:take_item()
	return itemstack
end

--modAirBoat.node_on_rightclick = function(itemstack, placer, pointed_thing, colorID, stickerID)
modAirBoat.node_on_rightclick = function(pos, node, clicker, itemstack, pointed_thing, colorID, stickerID)
	local playername = clicker:get_player_name()
	local meta = core.get_meta(pos)
	local ownername = meta:get_string("owner") or playername
	if not ownername or ownername == "" or ownername == playername
		--or core.get_player_privs(namePlayer).server
		--or core.get_player_privs(namePlayer).mayor
		or (
			core.get_modpath("tradelands")
			and modTradeLands~=nil
			and modTradeLands.getOwnerName(pos)~=""
			and modTradeLands.canInteract(pos, playername)
		)
		or (
			core.get_modpath("landrush")
			and landrush~=nil
			and landrush.get_owner(pos)~=nil
			and landrush.can_interact(pos, playername)
		)
	then
		local license_plate = meta:get_string("license_plate") or modAirBoat.getRandomLicensePlate(os.time()+math.random(1, 9999))
		local odometer = meta:get_string("odometer") or 0
		local nametag = meta:get_string("nametag") or ""
		local fuel = meta:get_string("fuel") or 0
		local gearshifter = meta:get_string("gearshifter") or 1
		local color = meta:get_string("color") or colorID
		local sticker = meta:get_string("sticker") or stickerID
		local seized = meta:get_string("seized") == "true"
		
		--modAirBoat.debug("[airboat][modAirBoat.node_on_place] fuel = "..dump(fuel))
		
		local tmpDatabase = {
			owner = ownername,
			license_plate = license_plate,
			color = color,
			sticker = sticker,
			odometer = odometer,
			nametag = nametag,
			fuel = fuel,
			gearshifter = gearshifter,
			seized = seized,
			--description = core.colorize("#00FF00",modAirBoat.translate("AIRBOAT @1", colorName))
			--[[
			description = string.format("%s %s", modAirBoat.getStickerName_by_StickerID(sticker), modAirBoat.getColorName_by_ColorID(color))				
			.."\n * "..modAirBoat.translate("License Plate: @1", core.colorize("#FFFF00",license_plate))
			.."\n * "..modAirBoat.translate("Owner: @1", core.colorize("#FFFF00",owner))
			.."\n * "..modAirBoat.translate("Odometer: @1", core.colorize("#FFFF00",string.format("%02.1f Km", odometer/1000)))
			--]]
		} 
		
		local staticdata = core.serialize(tmpDatabase) --returns a string
		local posEntity = {
			x = pos.x, 
			y = pos.y + (modAirBoat.scale), 
			z = pos.z
		}
		local obj = core.add_entity(
			posEntity,
			"airboat:airboat"
			, staticdata
		)	
		core.remove_node(pos)
		--modAirBoat.debug("[airboat][modAirBoat.node_on_place] staticdata = "..dump(staticdata))
		if obj then
			obj:set_yaw(clicker:get_look_horizontal())
			--[[ 
			--Utiliza outro método para  escrever
			obj:get_luaentity().owner = ownername
			obj:get_luaentity().license_plate = license_plate
			obj:get_luaentity().color = color
			obj:get_luaentity().sticker = sticker
			--obj:get_luaentity().fuel = fuel
			--obj:get_luaentity().gearshifter = gearshifter
			obj:get_luaentity().nametag = nametag
			obj:get_luaentity().seized = seized
			--[[ --]]
			--modAirBoat.debug("data = "..dump(data))
			--modAirBoat.debug("data.fuel = "..data.fuel)
			
			obj:set_properties({
			   owner = ownername,
			   license_plate = license_plate,
			   color = color,
			   sticker = sticker,
			   nametag = nametag,
			   seized = seized,
			   fuel = fuel,
			   gearshifter = gearshifter,
				infotext = string.format("%s %s", modAirBoat.getStickerName_by_StickerID(stickerID), modAirBoat.getColorName_by_ColorID(colorID))
					.."\n * "..modAirBoat.translate("Owner: @1", ownername)
					.."\n * "..modAirBoat.translate("License Plate: @1", license_plate),
				textures = {"airboat:nodebox_"..color.."_"..sticker},
				nametag_color = modAirBoat.def.config.nametag_color,
			})
		end
		modAirBoat.doSoundPlay(obj, "sfx_panel_click")
	else
		modAirBoat.send_message(nil, playername, 
			modAirBoat.translate(
				"This Airboat belongs to '@1'!"
				,core.colorize("#FFFF00", ownername)
			), "sfx_panel_fail"
		)
	end
	
	return false
end

modAirBoat.getColorName_by_ColorID = function(colorID)
	for i, row in ipairs(modAirBoat.colors) do
		if row[1] == colorID then
			return row[3]
		end
	end
	return ""
end

modAirBoat.getStickerName_by_StickerID = function(stickerID)
	for i, row in ipairs(modAirBoat.stickers) do
		if row[1] == stickerID then
			return row[3]
		end
	end
	return ""
end

modAirBoat.colors = {
	{"white",      "",			modAirBoat.translate("WHITE")			},
	{"grey",       "#888888",	modAirBoat.translate("GREY")			},
	{"dark_grey",  "#666666",	modAirBoat.translate("DARK GREY")	},
	{"black",      "#444444",	modAirBoat.translate("BLACK")			},
	{"violet",     "#8800ff",	modAirBoat.translate("VIOLET")		},
	{"blue",       "#0000ff",	modAirBoat.translate("BLUE")			},
	{"cyan",       "#8888ff",	modAirBoat.translate("CYAN")			},
	{"dark_green", "#006600",	modAirBoat.translate("DARK GREEN")	},
	{"green",      "#008800",	modAirBoat.translate("GREEN")			},
	{"yellow",     "#ffff00",	modAirBoat.translate("YELLOW")		},
	{"brown",      "#964B00",	modAirBoat.translate("BROWN")			}, --dark brown: 5C4033
	{"orange",     "#ff8800",	modAirBoat.translate("ORANGE")		},
	{"red",        "#ff0000",	modAirBoat.translate("RED")			},
	{"magenta",    "#ff00ff",	modAirBoat.translate("MAGENTA")		},
	{"pink",       "#ff88ff",	modAirBoat.translate("PINK")			},
}

for i, row in ipairs(modAirBoat.colors) do
	local colorID = row[1]
	local colorCode = row[2]
	local colorName = row[3]
	for i2, row2 in ipairs(modAirBoat.stickers) do
		local stickerID = row2[1]
		local stickerTexture = row2[2]
		local stickerName = row2[3]
		local stickerRecipe = row2[4]
		
		if i == 1 then
			core.register_craftitem("airboat:sticker_"..stickerID, {
				description = modAirBoat.translate("@1 STICKER", stickerName),
				inventory_image = stickerTexture,
			})
			core.register_craft({
				output = "airboat:sticker_"..stickerID,
				recipe = stickerRecipe
			})
		end
	
		local groups = {
			airboat = 1,
			snappy=2, 
			choppy=2, 
			oddly_breakable_by_hand=2,
			dig_immediate = 3, --Pode se retirado do chão com facilidade.
			--crumbly = 1, --quebradiço:  despenca com qualquer pancada.
			--falling_node = 1, --Despenca no chão se mal empilhado.
			flammable = 300, --Pode ser usado como combustível.
			--attached_node=1,
		}
		if colorID ~= "white" or stickerID ~= "default" then
			groups["not_in_creative_inventory"] = 1
		end
		groups["color_" .. colorID] = 1
		groups["sticker_" .. stickerID] = 1
		-- Nodebox for entity wielditem visual
		--modAirBoat.debug("[for("..i..")] core.register_node('airboat:nodebox_"..colorID.."', {def})")
		core.register_node("airboat:nodebox_"..colorID.."_"..stickerID, {
			description = core.colorize("#00FF00",
				--modAirBoat.translate("@1 @2", stickerName, colorName)
				stickerName.." "..colorName
			)
			.."\n * "..core.formspec_escape(
				modAirBoat.translate("Hold the RUN key (or AUX key) and put in floor to maximize this vehicle.")
			),
			
			--inventory_image = "airboat:nodebox_"..colorID,
			--inventory_image = "icon_airboat_inv.png",
			--wield_rotation = {x = 15, y = 30, z = 45},
			--wield_offset = {x = 10, y = 20, z = 30},
			wield_scale = {x = 1, y = 1, z = 1}, --Default: {x = 4, y = 4, z = 4}
			liquids_pointable = true,
			
			tiles = modAirBoat.getTexture(colorCode, stickerID),
			use_texture_alpha = "clip", --Entity Value = true | Node Value = "clip"
			paramtype = "light",
			paramtype2 = "facedir",
			drawtype = "nodebox",
			node_box = {
				type = "fixed",
				fixed = airboat_format,
			},
			groups = groups,
			sounds = {
				place = "default_place_node_hard",
				footstep = {name = "default_glass_footstep", gain = 0.3},
				dig = {name = "default_glass_footstep", gain = 0.5},
				dug = "default_place_node_hard",
				place_failed = "sfx_panel_fail",
				fall = {name = "default_break_glass", gain = 1.0},
			},
			on_place = function(itemstack, placer, pointed_thing)
				--modAirBoat.debug("[for("..i..")] modAirBoat.node_on_place(self, colorID='"..colorID.."', itemstack, placer, pointed_thing)")
				return modAirBoat.node_on_place(itemstack, placer, pointed_thing, colorID, stickerID)
			end,
			can_dig = function(pos, player)
				return modAirBoat.node_can_dig(pos, player, colorID, stickerID)
			end,
			on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
				return modAirBoat.node_on_rightclick(pos, node, clicker, itemstack, pointed_thing, colorID, stickerID)
			end,
		})
	end --from: for i2, row2 in ipairs(modAirBoat.stickers) do
end --from: for i, row in ipairs(modAirBoat.colors) do

if not core.get_modpath("components") then
	core.register_craft({
		output = "airboat:nodebox_white_default",
		recipe = {
			{"wool:white",		"wool:white",								"wool:white"},
			{"wool:white",		"airboat:nodebox_white_default",		"wool:white"},
			{"",					"default:steelblock",					"doors:door_steel"},
		}
	})
else
	core.register_craft({
		output = "airboat:nodebox_white_default",
		recipe = {
			{"airboat:sticker_default",	"components:ballon_dirigible",	"components:helice_blades_4"},
			{"components:cockpit", 			"components:engine_combustion",	"doors:door_steel"},
			{"default:steelblock", 			"default:steelblock",				"default:steelblock"}
		}
	})
end

core.register_alias("airboat","airboat:nodebox_white_default")
core.register_alias("dirigivel","airboat:nodebox_white_default")
core.register_alias(modAirBoat.translate("airboat"),"airboat:nodebox_white_default")

if core.get_modpath("tubelib_addons1") and core.get_modpath("biofuel") then
	core.register_craft({
		output = "biofuel:phial_fuel",
		recipe = {
			{"tubelib_addons1:biofuel",	"tubelib_addons1:biofuel",		"tubelib_addons1:biofuel"},
			{"tubelib_addons1:biofuel", 	"tubelib_addons1:biofuel",		""},
			{"", 									"",									""}
		}
	})
	core.register_craft({
		output = "tubelib_addons1:biofuel 5",
		recipe = {
			{"biofuel:phial_fuel",	"",	""},
			{"", 	"",	""},
			{"", 	"",	""}
		}
	})
end

