modAirBoat = {
	modname = core.get_current_modname(),
	modpath = core.get_modpath(core.get_current_modname()),
	scale = 5.0, --ideal size of airboat 1.5 
	px = 0.020833333,  --largura do pixel (= 1/48 pixels)
	--mainChair = {},
	handlerSound = { },
	entityFormSpec = { }, 
}

--dofile(core.get_modpath(modAirBoat.modname).."/config.lua")
dofile(core.get_modpath(modAirBoat.modname).."/translate.lua")
dofile(core.get_modpath(modAirBoat.modname).."/stickers.lua")
dofile(core.get_modpath(modAirBoat.modname).."/api.lua")
dofile(core.get_modpath(modAirBoat.modname).."/formspecs.lua")



core.register_entity(
	modAirBoat.get_entity()
)

core.register_on_player_receive_fields(function(sender, formname, fields)
	modAirBoat.on_player_receive_fields(sender, formname, fields)
end)

--[[ 
core.register_on_joinplayer(function(player, last_login)
	modAirBoat.debug("player:"..dump(player:get_player_name()).." attach:"..dump(attach))
	local attach = player:get_attach()
	if type(attach)=="nil" or type(attach:get_luaentity())=="nil" then
		modAirBoat.hudDelete(player)
	else
		local luaentity = attach:get_luaentity()
		if type(luaentity.drivername)=="string" and luaentity.drivername~="" then
			--modAirBoat.hudDraw(self, player, isVisible)
			modAirBoat.hudDraw(luaentity, player, true)
		end
	end
end)
--[[ ]]


core.register_on_leaveplayer(function(player, timed_out)
	modAirBoat.hudDelete(player)
end)

core.log('action',"["..modAirBoat.modname:upper().."] Loaded!")
