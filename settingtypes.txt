# AIBOAT SETTINGS
# Samples: https://github.com/minetest/minetest/blob/master/builtin/settingtypes.txt

# SHOW DEBUG:
# Allows you to print the debug 
# information of this mod on the 
# screen. Resource indicated only 
# for code programmers.
# Default: false
airboat.debug (Show debug) bool false

# MAXIMUM FLIGHT HEIGHT:
# Sets the maximum flight height allowed 
# by the airboat.
# Default: 1000 | Min: 100 | Max: 32000
airboat.max_flight_height (Maximum flight height) int 1000 100 32000

# HORN SOUND:
# Enables the horn feature 
# for all players.
# Default: true
airboat.sound_horn_enabled (Horn Sound Enabled) bool true

# HORN SOUND INTEEVAL:
# Time interval in seconds that a horn 
# can be sounded.
# Default: 5 | Min: 0 | Max: 99999999
airboat.sound_horn_interval (Sound Horn Interval) int 5 0 99999999

# ENCAPSULATER:
# Who owns the airboat 
# encapsulation feature.
# Default: public_scrivener
airboat.encapsulater (Encapsulation feature used only by) enum encapsulater encapsulater,public_notary,owner,police_officer,valet_parking,vehicle_mechanic





