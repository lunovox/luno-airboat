Tipo de Adesivo:
* [x] Padrão
* [x] Policial
* [x] Mecânico
* [x] Médico (SAMU)
* [x] FIREFIGHTER (SAMU)
* [x] TAXI (Para Fretistas)
* [x] EXPLORER (Para Exploradores)
* [ ] TRADER (Para Comerciantes)
* [ ] Racer (Corredor/Competidor/Racer)
* [ ] Limusine (para que é rico)
* [ ] Factory (para que tem Fábrica)
* [ ] Farmer (para quem tem fazenda)
* [ ] Builder (Construtor)
* [ ] Beatfull (para garotas Meigas)
* [ ] Warrior (Um boneto do tmw com armadura)
* [ ] CURSE (para adoradores do Curse)
* [ ] MASTER (para adoradores do Dungeon Master)
* [ ] OERKKI (para adoradores do Oerkki)

Form: Main Externo (Se fora do veículo)
* Recolher Veículo como Ítem (priv: server)
* Entrar Como Piloto
* Entrar Como Carona


Form: Main Interno (Se já Dentro)
* Manual do Usuário;
	* Cuidados com o Veículo
		* Combustivel
		* Sujeira
		* Velocidade de Colisão
		* Possíveis Defeitos
	* Atalhos
		* W : Frente
		* S : Ré, Desativa Modo Cruzeiro, e Desativar Auto-Piloto.
		* W + S : Mesmo que [RMB] no PC, ou [Double Click] nos Dispositivos Móveis.
		* A : Gira para Esquerda
		* D : Gira para Direita
		* A + D : Bozina
		* Espaço : Subir Balão
		* Shift : Descer Balão
		* Shift + Espaço : Dar Ignição, Parar Ignição
		* Aux : Gira em direção ao mouse;
		* Aux + W : Ativa Modo Cruzeiro (acelerador fixo);
		* Aux + S : Freio de Emergência, Desativa Modo Cruzeiro, e Desativar Auto-Piloto.
		* Aux + A : Desloca para Esquerda
		* Aux + D : Desloxa para Direita
		* Aux + Espaço : Elevar a Marcha
		* Aux + Shift : Reduzir a Marcha
		* Aux + Shift + Espaço : [A Definir]
* Dados do Veículo:
  * Placa de Identificação
  * GPS e Direção
  * Odometro : Cada 01km excedido além de 200k é +1% de chance de defeito será sorteada cada novo quilometragem.
  * Sugeira: Lavado com um balde com água;
    * 15% : se acima de 50% começa a perder integrigade.
  * Integridade: somente o mecânico repara com kit de reparo.
    * 85% : Se abaixo de 05%, Alguma peça quebra a cada kilometro.
    * Defeitos: somente visível se tiver o privilégio "mecânico".
      * Nenhum : Voa Normalmente;
      * Alternador : Não Reenche Bateria;
      * Bateria (Vazia): Não Bozina e nem da partida; precisa de trocar operador;
      * Bateria (Quebrada): Não Bozina e nem da partida; precisa trocar a bateria;
      * Acelerador : Não acelera;
      * Freio : Não Desacelera, nem dar ré;
      * Porta Emperrada: Não deixa ninguêm entrar;
      * Mala Emperrada : Não deixa guardar ou retirar itens;
      * Volante : Não Vira, nem desloca lateralmente;
      * Balão Furado : Não Sobe;
      * Vela Queimada : Não da Partida;
      * Tanque Furado : Vaza Combustivel;
      * Bozina : Não Toca; preciso criar uma chave de fenda;
      * Câmbio : não troca de marcha;
* Dar Partida;

* Cambio: (Se bater até 04m/s não quebra)
  * Marcha 4 (Max:15m/s)
  * Marcha 3 (Max:10m/s)
  * Marcha 2 (Max:05m/s)
  * Marcha 1 (Max:03m/s)
* Configurações:
	* Enviar Localização se Estacionado Longe
* Upgrades:
	* Bozina;
	* Pontos de Interesse (auto-pilot)
		* Marcar;
		* Remover;
		* Voar até;
	* Forno Elétrico;  -- Talvez adicione;
	* Caixa de Correio;  -- Talvez adicione;
	* Posta-Malas;  -- Talvez adicione;
* Abrir para Carona:
  * Cadeira 1 (Aberta) com "Biel"
  * Cadeira 2 (Travada)
* Botão "Sair do Veículo";

-- Talvez adicione;
* Lista [Pilotos Convidados]; obs: ou será que é melhor utilizar chaves de ouro para convidados?
	* Adicionar
	* Remover
* Farol de Milha
