modAirBoat.stickers = {
	{"default", "text_airboat_sticker.default.png", 
		modAirBoat.translate("DEFAULT AIRBOAT"),	
		{
			{"dye:black",			"",	""},
			{"dye:white",			"",	""},
			{"default:paper",		"",	""},
		}
	},
	{"mechanic", "text_airboat_sticker.mechanic.png", 
		modAirBoat.translate("MECHANIC AIRBOAT"),	
		{
			{"dye:black",		"dye:brown",		"dye:white"},
			{"dye:cyan",		"dye:grey",			"dye:cyan"},
			{"dye:white",		"default:paper",	"dye:white"},
		}
	},
	{"police", "text_airboat_sticker.police.png", 	
		modAirBoat.translate("POLICE AIRBOAT"),	
		{
			{"dye:white",		"dye:blue",			"dye:white"},
			{"dye:violet",		"dye:black",		"dye:violet"},
			{"dye:white",		"default:paper",	"dye:white"},
		}
	},
	{"medice", "text_airboat_sticker.medice.png", 
		modAirBoat.translate("MEDICE AIRBOAT"),	
		{
			{"dye:red",		"dye:red",			"dye:red"},
			{"dye:white",	"dye:black",		"dye:white"},
			{"dye:red",		"default:paper",	"dye:blue"},
		}
	},
	{"firefighter", "text_airboat_sticker.firefighter.png", 
		modAirBoat.translate("FIREFIGHTER AIRBOAT"),	
		{
			{"dye:red",			"dye:brown",		"dye:yellow"},
			{"dye:yellow",		"dye:yellow",		"dye:red"},
			{"dye:black",		"default:paper",	"dye:red"},
		}
	},
	{"taxi", "text_airboat_sticker.taxi.png",	
		modAirBoat.translate("TAXI AIRBOAT"),	
		{
			{"dye:black",		"dye:yellow",		"dye:black"},
			{"dye:yellow",		"dye:black",		"dye:white"},
			{"dye:black",		"dye:yellow",		"default:paper"},
		}
	},
	{"explorer", "text_airboat_sticker.explorer.png", 
		modAirBoat.translate("EXPLORER AIRBOAT"),	
		{
			{"dye:black",		"dye:blue",			"dye:white"},
			{"dye:white",		"dye:yellow",		"dye:cyan"},
			{"dye:white",		"default:paper",	"dye:white"},
		}
	},	
}