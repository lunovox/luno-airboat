![screenshot]

Other Screenshots: [02]  [03]  [04]  [05]  [06]

# [AIRBOAT] (Lunovox Version)

[![minetest_icon]][minetest_link] Adds an Airboat (type: airship balloon), in the version remade by **Lunovox Heavenfinder** from the version by **Paramat**.

The Luno-Airboat has high speed, with 4 gear levels, plus 1 reverse gear. It has a HUD with an indicator of: GPS (for Exploration), Altitude, Direction, Gear Levels, Speed, Fuel Level ("coal" in the default, but "Bio Fuel" in the extra mod), Odometer (which records the aging of the vehicle), and Clock. First person camera mode is recommended when traveling for a better view. The Airboat can be placed on any node, including liquids. It can land and float in a liquid. It has owner recognition (but can be directed by anyone who has the "valet_parking" privilege). Has the sound of the truck horn (can be disabled in 'minetest.conf' settings). Can be collected as an item into the inventory. It has a driver's seat and two passenger seats (You can lock the door to prevent passengers entry). Lightweight functionality, very suitable for online minetest servers.

## ⌛ In the Future the Luno-Airboat Will:
* Electric Oven (Disableable by "minetest.conf".)
* Message Box Receiving Terminal. (If mod "mail". Disableable by "minetest.conf".)
* Be Self-Piloted to specific coordinate. (Disableable by "minetest.conf".)
* Transport Other Vehicles. (if you have "official_police" privilege.)
* Have a trunk.
* High Speed Collision Defect Functionality. (Disableable by "minetest.conf".)
* Dirt Functionality. (Disableable by "minetest.conf".)
* Functionality of Defect in Various Components due to Vehicle Aging. (Disableable by "minetest.conf".)
* Repair Functionality in Various Components. (If you have "official_mechanic" privilege.)
* Mile Flashlight (Disableable by "minetest.conf".)

## 📦 **Dependencies:**

| Mod Name | Dependency Type | Descryption |
| :--: | :--: | :-- |
| default | Mandatory |  [Minetest Game] Included. | 
| doors | Mandatory | [Minetest Game] Included. |
| dye | Mandatory | [Minetest Game] Included. |
| flowers | Mandatory | [Minetest Game] Included. |
| keys | Mandatory | [Minetest Game] Included. |
| wool | Mandatory | [Minetest Game] Included. |
| [components] | Optional | To activate recipe complexity. | 
| [biofuel] | Optional | It allows the production of biofuel to power vehicles. | 
| [tradelands] or [landrush] | Optional | Allows you to remove the encapsulated Airboat from within its protected land. The Airboat (Lunovox version) is compatible with either of these two mods.  But I recommend 'tradelands' because it was created by the same creator as this vehicle. | 

## 📜 **License:**

* [![license_code_icon]][license_code_link]
* [![license_media_icon]][license_media_link]

See more details in wiki: 
 
[![wiki_en_icon]][wiki_en_link] [![wiki_pt_icon]][wiki_pt_link]

## ️🔬 **Developers:**

* Paramat: [Original Mod @0.2.1](https://github.com/paramat/airboat)
* Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [social web](http:qoto.org/@lunovox), [webchat](https://cloud.disroot.org/call/9aa2t7ib), [xmpp](https://conversations.im/i/lunovox@disroot.org?omemo-sid-633434523=97fce936651d6c3a28ca303ad5955ea2f1ab57fd5cd0769f04aebef6dd4cba79), [more contacts](https://libreplanet.org/wiki/User:Lunovox)

## 💾 **Downloads:**

* [Stable Versions] : There are several versions in different stages of completion.  Usually recommended for servers because they are more secure.

* [Git Repository] : It is the version currently under construction.  There may be some stability if used on servers.  It is only recommended for testers and mod developers.

## 📐 Recipes

### Result **without** opitional mod 'components':

![recipe]

Wool White, Wool Blue, Wool Yellow, Steel Block

### Result **with** opitional mod 'components':

![recipe2]

Much more difficult to craft. Right?

## 🎮 Controls

| Key | Descryption |
| :--: | :-- |
| <kbd>RMB/[Double Click in Screen]</kbd> | Enter or exit airboat when pointing at airboat. |
| <kbd>W/UP</kbd> | Speed to Forward. |
| <kbd>S/DOWN</kbd> | Speed to Backward. |
| <kbd>W/UP</kbd> + <kbd>S/DOWN</kbd> | Same as [RMB] on PC, or [Double Click] on Mobile Devices. |
| <kbd>A/LEFT</kbd> | Turn left. |
| <kbd>D/RIGHT</kbd> | Turn right. |
| <kbd>A/LEFT</kbd> + <kbd>D/RIGHT</kbd> | Horn Sound. |
| <kbd>JUMP/SPACE</kbd> | Ascend. |
| <kbd>SNEAK/SHIFT</kbd> | Descend. |
| <kbd>JUMP/SPACE</kbd> + <kbd>SNEAK/SHIFT</kbd> |  |
| <kbd>R/AUX</kbd> | Turn the vehicle to direction at mouse; |
| <kbd>R/AUX</kbd> + <kbd>W/UP</kbd> | Activates Cruise Mode (fixed throttle); |
| <kbd>R/AUX</kbd> + <kbd>S/DOWN</kbd> | Emergency Brake. |
| <kbd>R/AUX</kbd> + <kbd>W/UP</kbd> + <kbd>S/DOWN</kbd> |  |
| <kbd>R/AUX</kbd> + <kbd>A/LEFT</kbd> | Left-Side Move. |
| <kbd>R/AUX</kbd> + <kbd>D/RIGHT</kbd> | Right-Side Move. |
| <kbd>R/AUX</kbd> + <kbd>A/LEFT</kbd> + <kbd>D/RIGHT</kbd> |  |
| <kbd>R/AUX</kbd> + <kbd>JUMP/SPACE</kbd> | Ascend Gearbox Level. |
| <kbd>R/AUX</kbd> + <kbd>SNEAK/SHIFT</kbd> | Descend Gearbox Level. |
| <kbd>R/AUX</kbd> + <kbd>JUMP/SPACE</kbd> + <kbd>SNEAK/SHIFT</kbd> |  |

## 🌍 **Internacionalization:**

### **Available Languages:**

* 🇬🇧 Generic English [Default, Concluded: 100%]
* 🇵🇹 Português Genérico (Concluded: 99%)
* 🇧🇷 Português Brasileiro (Concluded: 99%)

  **Translate this mod to your Language:** See more details in file '[locale/README.md]'.

## 🌈 **Special Privileges:**

| Privilege | Descryption |
| :--: | :-- |
| ````valet_parking````		| 🙋 : Allows you to pilot another owner's Airboat. (Suitable only for "Police Officer" Players.) |
| ````vehicle_mechanic```` | 🧑‍🔧 : Allows you to modify and repair vehicles. |
| ````police_officer````	| 👮 : Allows to seize (blocking the control) of vehicles used in a harmful way. |
| ````public_notary````		| 🧑‍💼 : Allows to transfer ownership of a vehicle to another owner. |
| ````encapsulater````		| 🧑‍💻 : Allows you to encapsulate (transform into an item) a vehicle to transport it in the inventory. |

## ⚙️ **Settings:**

In **minetest.conf** file:

You don't need to do any of these presets mentioned below to make this mod work. But it's worth knowing the configuration values in case you want to directly change the ````minetest.conf```` file.

| Settings | Descryption |
| :-- | :-- |
| ````airboat.encapsulater = <value>```` | Who owns the airboat encapsulation feature. Values: ```aencapsulater````, ````public_notary````, ````owner````, ````police_officer````, ````valet_parking````, or ````vehicle_mechanic````; Default: ````encapsulater````. |
| ````airboat.sound_horn_enabled = <boolean>```` | Enables the horn feature for all players. Default: ````true````. |
| ````airboat.sound_horn_interval = <number>```` | Time interval in seconds that a horn can be sounded. Default: ````5```` ; Min: ````0```` ; Max: ````99999999````. |
| ````airboat.debug = <boolean>````		| If show debug info of this mod. Only util to developers. Default: ````false````. | 

# Have a good takeoff! 🌅
![favicon]

[screenshot]:./screenshot.png
[02]:./screenshot.2.png
[03]:./screenshot.3.png
[04]:#PortaExterna
[05]:#ManualDoUsuario
[06]:#AvisoDeTerrenoInseguro
[biofuel]:https://content.luanti.org/packages/Lokrates/biofuel/
[CHANGELOG.md]:https://gitlab.com/lunovox/luno-airboat/-/blob/master/CHANGELOG.md
[components]:https://gitlab.com/lunovox/components
[favicon]:./textures/icon_airboat_inv.png
[Git Repository]:https://gitlab.com/lunovox/luno-airboat
[intllib]:https://github.com/minetest-mods/intllib
[landrush]:https://content.luanti.org/packages/BrandonReese/landrush/?protocol_version=42
[license_code_icon]:https://img.shields.io/static/v1?label=LICENSE%20CODE&message=GNU%20AGPL%20v3.0&color=yellow
[license_code_link]:./LICENSE_CODE
[license_media_icon]:https://img.shields.io/static/v1?label=LICENSE%20MEDIA&message=CC%20BY-SA-4.0&color=yellow
[license_media_link]:./LICENSE_MEDIA
[locale/README.md]:https://gitlab.com/lunovox/luno-airboat/-/tree/master/locale?ref_type=heads
[AIRBOAT]:https://gitlab.com/lunovox/luno-airboat
[minetest_icon]:https://img.shields.io/static/v1?label=Minetest&message=Mod&color=brightgreen
[minetest_link]:https://luanti.org
[Minetest Game]:https://content.luanti.org/packages/Minetest/minetest_game/
[recipe]:./screenshot_recipe.png
[recipe2]:./screenshot_recipe.2.png
[Stable Versions]:https://gitlab.com/lunovox/luno-airboat/-/tags
[tradelands]:https://gitlab.com/lunovox/tradelands
[wiki_en_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=EN&color=blue
[wiki_en_link]:https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License
[wiki_pt_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=PT&color=blue
[wiki_pt_link]:https://pt.wikipedia.org/wiki/GNU_Affero_General_Public_License

